+++
title = "Application of Chow Rings -- Locus of Reducible Cubics"
date = Date(2021, 07, 08)
+++
@def desc = "An application of Chow rings to investigate the geometry of reducible cubics."
@def tags = ["summer_project", "intersection_theory"]

# Application of Chow Rings -- Locus of Reducible Cubics

The set of projective planar cubics is naturally parameterised by $\Pb^{9}$. In
this space, the collection of smooth cubics is open. To see this, we shall
prove a more general fact.

---

\lemma{Singular locus of degree $d$ hypersurfaces}{Let $n, d \in \N$ and $N
= \binom{n+d}{n-1}-1$, so $\Pb^{N}$ is the space of all degree $d$
hypersurfaces in $\Pb^{n}$. Let $\Gamma \subseteq \Pb^{N}$ be the locus of the
singular hypersurfaces. Then, $\Gamma$ is closed.}

*Proof:* Let $$\Sigma = \{(F, p) \in \Pb^{N} \times \Pb^{n} \mid \text{$p$ is
a singular point of $V(F)$}\},$$ with $\Pb^{N}$ being though of as the space of
$d$-forms. Given any $d$-form $F = \sum_{I}a_{I}X^{I}$, we can think of it as
being bihomogeneous of degree $(1, d)$ in the coordinates $a_{I}$ of $\Pb^{N}$
and $X_{0}, \dots, X_{n}$ of $\Pb^{n}$. Hence, the equations \begin{align} F &=
0 \\ \frac{\partial F}{\partial X_{i}} &= 0 \end{align} are all bihomogeneous
for $i = 0, \dots, n$. Note that the common zero locus of these equations is
precisely $\Sigma$, so $\Sigma$ is a closed subset of $\Pb^{N}$. As the
projection $\pi: \Pb^{N}\times \Pb^{n} \to \Pb^{N}$ onto the first coordinate
is proper, we see that $\pi(\Sigma) = \Gamma$ is closed as desired. $\square$

---

Among the singular plane cubics there is also some sort of a hierarchy. Let us
denote the locus of singular plane cubics by $\Gamma$. Using a similar
technique as the lemma above, one can show that the set of singular plane
cubics without nodes is closed. Now, also note that the set of reducible plane
cubics is closed. Indeed, the set of reducible plane cubics can be identified
as the image of $\tau: \Pb^{2}\times\Pb^{5} \to \Pb^{9}$, defined by first
treating $\Pb^{2}$ as the space of linear forms, $\Pb^{5}$ the space of
quadratic forms, and then taking their product. As the image of a projective
variety is closed, this locus is closed as well. This tells us that the set of
nodal cubics is open in $\Gamma$, and has dimension 8.

There is a further hierarchy within the locus of reducible/cuspidal cubics. The
following diagram taken from [Eis] shows what exactly is going on.

\figenv{Hierarchy of cubics}{/media/hierarchy_pcc.png}{width=90%}

Using mostly similar arguments, one can check that the specified dimension and
specialisation relationships do in fact hold. It is nonetheless still
interesting to compute certain intersection theoretic attributes of these
classes.

## The Reducible Cubics

As said previously, the locus of reducible cubics can be seen as the image
$\Gamma$ of the multiplication map $\tau: \Pb^{2} \times \Pb^{5} \to \Pb^{9}$,
which is a closed subvariety of dimension 7. We are interested in computing the
class and degree of $\Gamma$ in $A(\Pb^{9})$. As the class of a pure
dimensional irreducible subvariety is uniquely determined by its dimension and
degree, it suffices to determine $\deg(\Gamma)$.

Let $\{X_{i}\}, \{Y_{i}\}, \{Z_{i}\}$ be homogeneous coordinates of $\Pb^{2},
\Pb^{5}$, and $\Pb^{9}$ respectively. Suppose also that $Z_{0}$ corresponds to
the coefficient of $X^{3}$, $X_{0}$ that of $X$, and $Y_{0}$ that of $X^{2}$.
Consider the hyperplane $P = V(Z_{0}) \subseteq \Pb^{9}$. It is easy to check
that its preimage under $\tau$ is $V(X_{0})\cup V(Y_{0}) \subseteq
\Pb^{2}\times \Pb^{5}$. Letting $\xi$ denote the class of $P$, $\alpha$ the
pullback of a hyperplane class in $\Pb^{2}$ and $\beta$ the pullback of
a hyperplane class in $\Pb^{5}$, we see that $\tau^{\ast}(\xi) = \alpha
+ \beta$.

Now, by unique factorisation of polynomials, away from the locus of product of
lines, the fibres of $\tau$ has cardinality 1. Intuitively, general hyperplanes
intersect $\Gamma$ away from this locus. To show this rigorously, denote this
locus by $\Lambda$. Define the incidence scheme $$I = \{(\Pi, p) \in
\Pb^{9\ast}\times\Lambda \mid p \in \Pi\}.$$ One can easily show that this is
a closed subvariety of $\Pb^{9\ast}\times\Pb^{9}$. Hence, its projection
$\pi_{1}(I)$ to $\Pb^{9\ast}$ is closed. But $\pi_{1}(I)$ is precisely the
collection of hyperplanes whose intersection with $\Gamma$ contains at least
one point from $\Lambda$. Its complement in $\Pb^{9\ast}$ is thus open. In
other words, a general hyperplane intersects $\Gamma$ outside of $\Lambda$.

This brings us to the final stage of the computation. We know that $\Gamma$ has
dimension $7$, so its degree is the cardinality of its intersection with $7$
general hyperplanes in $\Pb^{9}$. Since a general hyperplane intersects with
$\Gamma$ away from $\Lambda$, it follows that this cardinality is the same as
the intersection between $7$ general hyperplane pullbacks and $\Pb^{2} \times
\Pb^{5}$. This is given by $$\deg(\tau^{\ast}(\xi)^{7}) = \deg((\alpha
+ \beta)^{7}) = \deg(27\alpha^{2}\beta^{5}) = 27.$$ Coincidentally, this is
  also the number of lines on a smooth projective cubic surface.

This allows us to answer a question which seems completely unrelated. Let
$F_{0}, F_{1}, F_{2} \in k[X, Y, Z]$ be general cubics. Up to scalars, how many
linear combinations $t_{0}F_{0} + t_{1}F_{1} + t_{2}F_{2}$ are reducible? To
solve this, note that $F_{0}, F_{1}$ and $F_{2}$ spans a general $2$-plane in
$\Pb^{9}$, which is the intersection of $7$ general hyperplanes. The desired
number is the cardinality of the intersection between these hyperplanes and
$\Gamma$, which is $\deg(\Gamma) = 27$.

## The Triangles

The analysis is almost completely identical with the locus of triangles, $T$.
Here, a triangle means a completely reducible cubic. We can similarly describe
$T$ as the image of the map $\mu: \Pb^{2}\times\Pb^{2}\times\Pb^{2} \to
\Pb^{9}$, determined by multiplication. Let $\xi \in A(\Pb^{9})$ be
a hyperplane class. A similar analysis shows that $\mu^{\ast}(\xi) = \alpha_{1}
+ \alpha_{2} + \alpha_{3}$, where $\alpha_{i}$ stands for the pullback of the
hyperplane class in the $i$th factor along the projection map.

This time, a general fibre of $\mu$ has cardinality $6$, as any permutation of
the three linear factors gives the same product. This number is achieved when
the three linear factors are distinct, and one can easily show that the
complement of this is closed in $\Pb^{2}\times\Pb^{2}\times\Pb^{2}$. By the
same argument as in the previous section, the fibre along any point in the
intersection between a general hyperplane and $T$ has cardinality $6$. Noting
that $T$ has dimension $6$, this immediately implies that $$\deg(T)
= \frac{1}{6}\deg((\alpha_{1} + \alpha_{2} + \alpha_{3})^{6})
= \frac{1}{6}\deg(90\,\alpha_{1}^{2}\alpha_{2}^{2}\alpha_{3}^{2}) = 15.$$

Similar to the previous section, this fact also implies a enumerative result.
Given four general cubic forms in $k[X, Y, Z]$, up to scalars exactly $16$
elements in their span are completely reducible.

## The Asterisks

An asterisk is a cubic which occurs as the union of three concurrent lines.
This can be described as a subvariety of $\Pb^{2}\times\Pb^{2}\times\Pb^{2}$.
Indeed, if we write the $i$th line as the zero locus of the linear form $a_{i,
1}X + a_{i, 2}Y + a_{i, 3}Z$, we see that the three lines being concurrent is
equivalent to saying that the system $$(a_{i, j})_{i, j}\pmat{X\\Y\\ Z} = 0$$
as a nonzero solution, which is equivalent to saying that $\deg((a_{i, j})_{i,
j}) = 0$. This is a homogeneous trilinear form in the coefficients, and thus
defines a closed subvariety $\Phi$ of $\Pb^{2}\times\Pb^{2}\times\Pb^{2}$ whose class
is $$\alpha_{1} + \alpha_{2} + \alpha_{3} \in
A(\Pb^{2}\times\Pb^{2}\times\Pb^{2}).$$ The locus of asterisks is thus the
image $A$ of this set under $\mu$, and has dimension $5$.

This means that the pullback of five general hyperplanes will intersect $\Phi$
in $\deg([\Phi](\alpha_{1}+\alpha_{2}+\alpha_{3})^{5}) = 90$ points. Using the
same argument as in the previous section, we see that $\mu$ is generically
six-to-one, so $\deg(A) = 90/6 = 15$ as desired.
