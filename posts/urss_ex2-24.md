+++
title = "3264 Exercise 2.24"
date = Date(2021, 7, 11)
+++
@def desc = "Exercise 2.24 from 3264 and All That"
@def tags = ["summer_project", "intersection_theory"]

# 3264 Exercise 2.24

**Exercise 2.24** Let $\sigma = \sigma_{r, s}: \Pb^{r}\times\Pb^{s} \to \Pb^{rs
+ r + s}$ be the Segre map, and let $X \subseteq \Pb^{r}\times\Pb^{s}$ be
  a subvariety of codimension $k$. Let the class $[X] \in
  A^{k}(\Pb^{r}\times\Pb^{s})$ be given by $$[X] = \sum_{i=0}^{k}
  c_{i}\alpha^{k-i}\beta^{i}.$$ Here, $\alpha, \beta \in
  A^{1}(\Pb^{r}\times\Pb^{s})$ are the pullbacks of the hyperplane classes, and
  we take $c_{i} = 0$ whenever $i > s$ or $k-i > r$.

  1. Show that all $c_{i} \geq 0$.

  1. Calculate the degree of the image $\sigma(X) \subseteq \Pb^{rs+r+s}$.

  1. Using (1) and (2), show that any linear subspace $\Lambda \subseteq
     \Sigma_{r, s} \subseteq \Pb^{rs+r+s}$ contained in the Segre variety lies
     either in a fibre of the map $\Sigma_{r,s} \cong \Pb^{r}\times\Pb^{s} \to
     \Pb^{r}$ or the corresponding map to $\Pb^{s}$.

### Solution

#### Part 1

We have $c_{i} = \int \alpha^{r-k+i}\beta^{s-i} \cap [X]$.

We induct on $r + s$. Note that the cases where $rs = 0$ follows easily.
Similarly, the case follows if $X$ is contained in one of the fibres. Finally, the remaining cases follow by considering the cartesian square

~~~
<iframe class="quiver-embed" src="https://q.uiver.app/?q=WzAsNCxbMCwwLCJcXGFscGhhXFxjYXAgWCJdLFswLDEsIlgiXSxbMSwxLCJcXFNpZ21hX3tyLCBzfSJdLFsxLDAsIlxcU2lnbWFfe3ItMSwgc30iXSxbMSwyXSxbMywyXSxbMCwxXSxbMCwzXSxbMCwyLCIiLDEseyJzdHlsZSI6eyJuYW1lIjoiY29ybmVyIn19XV0=&embed" width="340" height="304" style="border-radius: 8px; border: none;"></iframe>
~~~

and applying the induction hypothesis.

#### Part 2

Let $\xi$ be the hyperplane class on $\Pb^{rs+r+s} := \Pb^{N}$. Calculating the
degree of $\sigma(X)$ amounts to computing the intersection product $$
\xi^{r+s-k} \cap \sigma_{\ast}[X] = \sigma_{\ast}(\sigma^{\ast}\xi^{r+s-k} \cap
[X])$$ by the projection formula. We know that $$ \sigma^{\ast}\xi^{r+s-k}
= (\alpha+\beta)^{r+s-k},$$ thus the intersection product (2) evaluates to $$
\left( \sum_{j = 0}^{r+s-k} \binom{r+s-k}{j} \alpha^{j}\beta^{r+s-k-j} \right)
\cap \sum_{i = 0}^{k} c_{i}\alpha^{k-i}\beta^{i}.$$ Taking the degree map, we
obtain the desired degree as $$ \sum_{i=0}^{k} \binom{r+s-k}{r-k+i} c_{i} .$$

#### Part 3

Since the coefficients $c_{i}$ are all non-negative and any such $\Lambda$
would have degree $1$, we know that exactly one of the $c_{i}$ is nonzero. In
fact, this sole nonzero coefficient would have to be equal to $1$. This means
that the accompanying binomial coefficient $\binom{r+s-k}{r-k+i}$ would be
forced to be equal to $1$. Hence, either $r - k + i = 0$ or $r
- k + i = r+s-k$. In the first case, we have $i = k - r$. Hence, the class of
  $X$ is $$ \alpha^{r}\beta^{k-r}.$$ The only subvarieties of $\Pb^{r}\times
  \Pb^{s}$ with class $\alpha$ are those occurring as fibres of projection onto
  the first coordinate. Since the pure binomials only occur as classes of
  products of linear subspaces, we see that $X$ is contained in a fibre of the
  projection to the second component. The other case proceeds similarly.
