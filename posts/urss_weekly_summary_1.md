+++
title = "Weekly Journal: 2021/07/05-2021/07/11"
date = Date(2021, 07, 06)
+++
@def desc = "Weekly update on matters related to my summer project"
@def tags = ["summer_project"]

# Weekly Journal: 2021/07/05-2021/07/11

This is officially the first week of my summer project. Below I will detail
this week's meeting with my supervisor, and set out what I would like to do
this week.

\toc

## Meeting with Supervisor

The discussion this week centres mainly around two matters. In particular,
I clarified a 'conjecture' I had with my supervisor, and he also answered
a soft question I posted.

### The conjecture

In [this post](/posts/urss_roadmap), I made the claim that the expected
codimension of $F_{k}(X)$ and the actual dimension of $\G(k, n + 1)$ gives an
indication as to whether we expect an infinite number of $k$-planes on $X$ or
none at all. This conjecture turns out to be partially correct.

In the case where the expected codimension of $F_{k}(X)$ is larger than the
dimension of $\G(k, n+1)$, I conjectured that there will be no $k$-planes on
$X$. This turns out to be false. Consider, for a counter example, the Fermat
quartic $Q = V(X^{4} + Y^{4} + Z^{4} + W^{4}) \subseteq \Pb^{3}$. This is a
degree $4$ hypersurface in $\Pb^{3}$, so the expected codimension of $F_{1}(Q)$
is $\binom{1+4}{1} = 5$. The dimension of $\G(2, 4)$, however, is $2(4-2) = 4$,
so according to my conjecture $Q$ should not contain any lines. This is clearly
false: for instance, clearly $V(X-\sqrt{i}Y, Z-\sqrt{i}W) \subseteq Q$
(assuming that our base field is algebraically closed, and $i$ is a square root
of $-1$.). In fact, a bit more can be said.

\prop{Lines on the Fermat Quartic}{There are exactly 48 lines on the Fermat
quartic $$Q = V(X^{4} + Y^{4} + Z^{4} + W^{4}) \subseteq \Pb^{3}.$$}

*Proof:* We first find 48 lines on $Q$. A simple linear algebraic argument
shows that $V(X-i^{(2k+1)/2}Y, Z-i^{(2\ell+1)/2}W)$ defines a distinct plane
for each of the pairs $0 \leq k, \ell \leq 3$. This gives a total of 16 lines.
Applying the permutations $X\leftrightarrow Z$ and $Y \leftrightarrow Z$ gives
$16$ distinct lines each, which gives a total of 48 lines.

Showing that these are the only lines is once again a linear algebraic process.
Consider any line $L$ contained in $Q$. As the family of lines we found above
are invariant under permutation of coordinates, we may assume after a
permutation that $L = V(X - aZ - bW, Y - cZ - dW)$ for some $a, b, c, d \in k$.
Substituting and expanding gives $a^{4}Z^{4} + 4a^{3}bZ^{3}W +
6a^{2}b^{2}Z^{2}W^{2} + 4ab^{3}ZW^{3} + b^{4}W^{4} + c^{4}Z^{4} + 4c^{3}dZ^{3}W
+ 6c^{2}d^{2}Z^{2}W^{2} + 4cd^{3}ZW^{3} + d^{4}W^{4} + Z^{4} + W^{4} = 0$. From
this, it follows that $a^{4} + c^{4} = b^{4} + d^{4} = -1$. Moreover, $a^{3}b -
c^{3}d =0$. Multiplying both sides by $ad$ gives $0 = a^{4}bd
- ac^{3}d^{2} = -bd -c^{4}bd -ac^{3}d^{2} = -bd -c^{4}bd -a^{3}b^{2}c$. Hence,
  $b(d + c^{4}d + a^{3}bc) = 0$ If $b = 0$, then $d^{4} = -1$, and thus $0 =
  ab^{3}d + cd^{4} = -c$, and so $a^{4} = -1$. This is one of the lines we
  found above.

If $b \neq 0$, then $d + c^{4}d + a^{3}bc = 0$. Hence, $0 = d + c^{4}d +
a^{3}bc = d + c^{4}d - c^{3}dc = d$. Thus, $b^{4} = -1$. Also, $a^{3}b = 0$,
thus $a = 0$, so $c^{4} = -1$. This is also one of the lines we found above.
$\square$

---

According to my supervisor, despite this counterexample the conjecture in this
case does actually hold in general. It is believed to be a standard fact in
enumerative geometry, and I should probably look it up some time.

The other case is also partially correct. By considering the incidence
subscheme of $B\times \G$, where $B$ is the base space of the family of $X$ and
$\G$ is the appropriate Grassmannian, it can be shown that the standard
projection onto the first coordinate is proper, and every point has a nonempty
image. Via some dimension consideration one can then show that at least one
point has an infinite fibre. I should probably look this up some time as well.

### The soft question

I also consulted with my supervisor on a question that caught my attention a
while ago. The question is, *given the rank of a vector bundle over a variety
$X$, what can be said about its global section*?

As an example, consider the simplest case of a line bundle over $\Pb^{N}$. We
know that a line bundle over $\Pb^{N}$ is isomorphic to $\Os(d)$ for some $d
\in \Z$. If $d < 0$, then $\Gamma(X, \Os(d)) = 0$. Else, we know that
$\dim\Gamma(X, \Os(d)) = \binom{N+d}{N+1}$, which is a numerical polynomial in
$d$. We can summarise this by saying that the allowed global dimension of a
line bundle over $\Pb^{N}$ is the range of a numerical polynomial.

It turns out that this problem is incredibly tricky, even if we restrict our
attention to line bundles over curves. As a start, suppose that a given curve
has genus $g$, and a given line bundle $\mcL$ over it has degree $d$ (that is,
the degree of its first Chern class). If $d < 0$, then $\mcL$ will have no
nontrivial global section. Indeed, we know that depending on whether a global
section vanishes, it will have a non-negative degree, and we know that the
choice of rational section does not affect the first Chern class.

When $d \geq 2g-2$, the possible global dimension of $\mcL$ is a well-known
result following from the classical Brill-Noether theory for curves. The real
interesting thing happens between the critical strip $0 \leq d < 2g-2$. By
Serre duality, we only have to consider the case $0 \leq d \leq g-1$. This is
an open problem in current research. The determination of global dimensions is
only done when $\mcL$ and the underlying curve are general, and little is known
about the non-general case. In fact, we do not even have a bound! It is widely
conjectured that a bound is achieved by considering bundles on hyperelliptic
curves, but the exact bound is still unknown.

This is an area of research which I find to be extremely interesting. After the
summer project I will certainly start learning the basics of sheaf cohomology
and Brill-Noether theory, and eventually I hope to work through Lazarsfeld's
monumental paper *Brill-Noether-Petri Without Degenerations*.

## Plans on the Upcoming Week

1. Work out 1-2 more examples of Chow rings. (Done)

    1. Locus of singular cubics [here](/posts/urss_singular_cubics)

    1. Chow ring of multiprojective spaces and degree of Segre varieties [here](/posts/urss_multiprojective)

3. Finish the post on divisors and Chern classes.

1. Finish at least Section 3.2 of 3264.

1. Work on ~3 exercises from Ch. 2.

