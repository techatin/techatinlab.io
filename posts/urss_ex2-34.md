+++
title = "3264 Exercise 2.34"
date = Date(2021, 07, 11)
+++
@def desc = "Exercise 2.34 from 3264 and All That"
@def tags = ["summer_project", "intersection_theory"]

# 3264 Exercise 2.34

**Exercise 2.34** Consider the locus $\Phi \subseteq (\Pb^{2})^{4}$ of 4-tuples
of colinear points. Find the class $\phi = [\Phi] \in A((\Pb^{2})^{4})$ of
$\Phi$ by method of undetermined coefficients.

### Solution

We first verify that $\Phi$ is indeed a subvariety. Given $1 \leq i \leq 4$,
let $\{X_{i, j}\}_{j=0,1,2}$ be the homogeneous coordinates on the $i$th factor
of $(\Pb^{2})^{4}$. The condition that four points are colinear is equivalent
to saying that the matrix $(X_{i, j})_{i, j}$ has rank at most 2. This is again
equivalent to saying that the determinants of the $2\times 2$ minors all
vanish. This gives $\Phi$ as the vanishing locus of 18 bihomogeneous quadrics.

We now determine the dimension of $\Phi$. Consider the projection $\pi: \Phi
\to (\Pb^{2})^{2}$ of $\Phi$ onto the first two factors. Given any $p \in
(\Pb^{2})^{2}$, clearly $\pi^{-1}(p) \cong (\Pb^{1})^{2}$ has dimension 2.
Thus, $\Phi$ is irreducible. Moreover, as every fibre has the same dimension we
conclude that $\dim \Phi = \dim(\Pb^{2})^{2} + 2 = 6$. This says that $\phi \in
A^{2}((\Pb^{2})^{4})$.

Now write $$\phi = \sum_{1\leq i\leq j \leq 4}^{} c_{i,
j}\alpha_{i}\alpha_{j},$$ where $c_{i, j} \in \Z$ and $\alpha_{i}$ is the
pullback of the hyperplane class from the $i$th factor. To compute the
coefficient $c_{i, j}$, intersect $\phi$ with
$\alpha_{1}^{2}\alpha_{2}^{2}\alpha_{3}^{2}\alpha_{4}^{2}/(\alpha_{i}\alpha_{j})$,
and the resulting class of the intersection is thus $$c_{i,
j}\alpha_{1}^{2}\alpha_{2}^{2}\alpha_{3}^{2}\alpha_{4}^{2},$$ whose degree is
$c_{i, j}$. By symmetry, we only have to compute $c_{1, 1}$ and $c_{1, 2}$.

To compute $c_{1, 1}$, we intersect $\phi$ with
$\alpha_{2}^{2}\alpha_{3}^{2}\alpha_{4}^{2}$. Consider any representative $C$
of the class. By restricting the rational equivalence to the first factor, one
sees that the pushforward of $C$ along $\pi_{1}$ is equivalent to a hyperplane
class. By dimension considerations, we thus know that $C$ a prime divisor of
the form $H \times \left\{\ast\right\} \times \left\{\ast\right\} \times
\left\{\ast\right\}$, where $H \subseteq \Pb^{2}$ is a hypersurface. Hence, it
is not possible to move $C$ in such a way that intersects $\phi$ generically
transversally should $c_{1, 1} \neq 0$. Thus, we must conclude that $c_{1, 1}
= 0$.

To compute $c_{1, 2}$, we intersect $\phi$ with
$\alpha_{1}\alpha_{2}\alpha_{3}^{2}\alpha_{4}^{2}$. The latter can be realised
as the class of the intersection $$V(X_{1, 0})\cap V(X_{2, 1})\cap V(X_{3, 0},
X_{3, 2}) \cap V(X_{4, 1}, X_{4, 2}).$$ This intersection of this with $\Phi$
quite clearly consists of the single point $([0:1:0], [1:0:0], [0:1:0],
[1:0:0])$, so $c_{1, 2} = 1$.

This shows that $$\phi = \sum_{1 \leq i < j \leq 4}^{} \alpha_{i}\alpha_{j}.$$
