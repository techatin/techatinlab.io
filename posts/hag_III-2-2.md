+++
title = "Hartshorne Exercise III.2.2"
date = Date(2021, 07, 23)
+++
@def desc = "Cohomology of the projective line"
@tags = ["hartshorne", "cohomology"]

# Hartshorne Exercise III.2.2

**Exercise III.2.2:** Let $X = \Pb^{1}_{k}$ be the projective line over an
algebraically closed field $k$. Show that the exact sequence $$0 \rightarrow
\mathscr{O} \rightarrow \mathscr{K} \rightarrow \mathscr{K}/\mathscr{O}
\rightarrow 0$$ is a flasque resolution of $\mathscr{O}$. Conclude from
(II.1.21e) that $H^{i}(X, \mathscr{O}) = 0$ for all $i > 0$.

*note.* The sheaf $\mathscr{K}$ here stands for the constant sheaf of rational
functions over $X$.

### Solution

To show that the given exact sequence is a flasque resolution of $\mathscr{O}$,
it suffices to show that $\mathscr{K}$ is flasque. We shall show once and for
all the more general result that a constant sheaf over a irreducible
topological space is flasque.

Let $Y$ be an irreducible topological space, and $\mathscr{C}$ a constant sheaf
over $Y$. If $U \subseteq Y$ is an open subset, then $U$ is irreducible, and
more specially connected. Hence, given open subsets $V \subseteq U \subseteq
Y$, we know that the restriction morphism $\rho_{U, V}$ must be surjective as
$V$ and $U$ have the same number of connected components. This shows that
$\mathscr{C}$ is flasque as desired.

That $\mathscr{K}/\mathscr{O}$ is flasque follows from the conclusion of
II.1.21d, which we shall prove here.

**Claim:** We have $\mathscr{K}/\mathscr{O} \cong \bigoplus_{p \in X} i_{p,
\ast}(K(X)/\mathscr{O}_{p})$.

*Proof:* Denote by $\mathscr{K}\|\mathscr{O}$ the *quotient presheaf* of
$\mathscr{K}$ by $\mathscr{O}$. Noting that a rational function only fails to
be regular at finitely many points on $\Pb^{1}$, we see that there is a natural
map of presheaves $\hat{\phi}: \mathscr{K}\|\mathscr{O} \to \bigoplus_{p \in X}
i_{p, \ast}(K(X)/\mathscr{O}_{p})$. This induces a unique map $\phi:
\mathscr{K}/\mathscr{O} \to \bigoplus_{p \in X} i_{p,
\ast}(K(X)/\mathscr{O}_{p}) = \mathscr{F}$.

To show that $\phi$ is indeed an isomorphism, we shall exhibit an inverse
$\psi$ of $\phi$. Given an element $s \in \mathscr{F}(U)$, note that it
vanishes at all but finitely points. Let $p$ be one of these points, and
$s_{p}$ the corresponding germ at $p$. Now, $s_{p}$ can be represented by
a section of $\mathscr{K}\|\mathscr{O}(U)$ which fails to be regular at $p$.
From $U$ remove $p$ and all other points for which the section fails to be
regular. Doing this for every $p$ defines a compatible system of germs. It is
then not hard to see that $\psi$ is indeed the inverse to $\phi$. $\square$

As each $i_{p, \ast}(K(X)/\mathscr{O}_{p})$ is a skyscraper sheaf, it is
flasque, so $\mathscr{F} \cong \mathscr{K}/\mathscr{O}$ being a direct sum of
flasque sheaves is flasque as well.

We now need to use the conclusion of II.1.21e, which we shall prove here. We
need to show that the map $\Gamma(X, \mathscr{K}) \to \Gamma(X,
\mathscr{K}/\mathscr{O})$ is surjective. We shall show this by leveraging the
isomorphism $\mathscr{K}/\mathscr{O} \cong \mathscr{F}$ we found earlier.
Consider a section $s \in \mathscr{F}(X)$. This section vanishes at all but
finitely many points, say $p_{1}, \dots, p_{n}$. By a projectivity, assume that
none these points are at infinity. By partial fractions decomposition, we can
choose a representative $f_{i} \in K(X)$ of $s_{p_{i}}$ which has a pole at
only $p_{i}$ and nowhere else. The function $\sum_{i}^{} f_{i} \in K(X)$ is
then mapped onto $s$.

With that, we may apply $\Gamma(X, \cdot)$ and get the long exact sequence $$0
\rightarrow \mathscr{O}(X) \to \mathscr{K}(X) \to \mathscr{K}/\mathscr{O}(X)
\to 0 \to H^{1}(X, \mathscr{K}) \to H^{1}(X, \mathscr{K}/\mathscr{O}) \to
\dots.$$

This tells us that $H^{1}(X, \mathscr{O}) = 0$. More generally, given any $i
> 1$ we would have the fragment $$\dots \to H^{i-1}(X, \mathscr{K}/\mathscr{O})
\to H^{i}(X, \mathscr{O}) \to H^{i}(X, \mathscr{K}) \to \dots.$$ The terms on
the two sides all vanish, since $\mathscr{K}$ and $\mathscr{K}/\mathscr{O}$ are
flasque. This forces $H^{i}(X, \mathscr{O}) = 0$ as desired.

$\mathcal{J}$
