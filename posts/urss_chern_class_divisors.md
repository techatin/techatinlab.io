+++
title = "First Chern Class of a Line Bundle and Divisors"
date = Date(2021, 07, 04)
+++
@def desc = "[Under construction] We look at the definition for the first Chern class of a line bundle, and how it gives a correspondence between the Picard group and the divisor class group."
@def tags = ["summer_project", "intersection_theory"]

# First Chern Class of a Line Bundle and Divisors

A fact familiar to the experts and puzzling to the amateurs is that line
bundles and divisors (Weil or Cartier) are very intimately linked. In
particular, to each line bundle we can associate a Cartier divisor via a construction known as the *first Chern class*.

The realisation comes from the fact that locally, a rational section of a line
bundle is a rational function. Let $X$ be a variety and $\mcL$ a line bundle on
$X$. Recall that this means that $\mcL$ is a locally free $\Os_{X}$-module of
rank 1. Take a nonzero rational section $\sigma$ of $\mcL$, and an affine open
cover $\mfU = \{U_{i}\}_{i \in I}$ of $X$ sufficiently fine so that $\mcL$ is
trivial over each $U_{i}$.

On each open subscheme $U_{i}$, the rational section $\sigma$ corresponds to
a rational function $f_{i}/g_{i}$ for some $f_{i}, g_{i} \in \Os_{X}(U_{i})$,
with $f_{i}/g_{i}$ and $f_{j}/g_{j}$ agreeing over the intersections $U_{i}
\cap U_{j}$. This defines a Cartier divisor. As the notions of Cartier divisors
and Weil divisors agree over locally factorial Noetherian integral schemes,
this Cartier divisor corresponds to a class in $A_{1}(X)$, denoted
$\div\sigma$.

Now, given any two rational sections $\sigma$ and $\tau$, note that
$\sigma/\tau$ is locally a rational function. Thus, the divisors represented by
$\sigma$ and $\tau$ differs by a principal divisor, thus they have the same
class in $A_{1}(X)$.

This class is called the *first Chern class* of $\mathcal{L}$, denoted
$c_{1}(\mathcal{L})$. This gives a first glimpse into how line bundles are
related to divisors.

\prop{Picard group and divisor class group}{The first Chern class gives a group
homomorphism $$c_{1}: \Pic(X) \to A_{1}(X).$$ This homomorphism is in fact an
isomorphism when $X$ is smooth.}

*Proof:* That $c_{1}$ takes the trivial bundle to $0$ is trivial. Thus, to show
that $c_{1}$ is a homomorphism of groups, we only have to verify that
$c_{1}([\mcL\otimes\mcL^{\prime}]) = c_{1}([\mcL]) + c_{1}([\mcL^{\prime}])$.
Let $\sigma$ be a rational section of $\mcL$, and $\sigma^{\prime}$ a rational
section of $\mcL^{\prime}$. Then, $\sigma\otimes\sigma^{\prime}$ is a rational
section of $\mcL\otimes\mcL^{\prime}$. Locally, a trivialisation of
$\mcL\otimes\mcL^{\prime}$ takes $\sigma\otimes\sigma^{\prime}$ to the product
of the trivialisation of $\sigma$ and $\sigma^{\prime}$. Hence,
$\div(\sigma\otimes\sigma^{\prime}) = \div\sigma + \div\sigma^{\prime}
= c_{1}(\mcL) + c_{1}(\mcL^{\prime})$ as desired.

Now, assume that $X$ is smooth and projective. We construct an inverse for $c_{1}$.

Let $Y\subseteq X$ be a closed codimension-1 subvariety. Let $\Os_{X}(-Y)$
denote the sheaf of ideals associated to $Y$. We wish to show that
$\Os_{X}(-Y)$ is locally free of rank 1. As being locally free of rank 1 is an
affine local property, it suffices to prove this for the case $X = \Spec A$ for
a finitely generated integral $k$-algebra $A$. We now take a short interlude to
prove this case.

\lemma{Codimension-1 prime ideal sheaf is locally free}{Let $\Gamma$ be a
finitely generated regular $k$-algebra, and $\mfp \id \Gamma$ a codimension-1
prime ideal. Then, there exists some $g_{1}, g_{2}, \dots, g_{n} \in
\Gamma^{\ast}$ such that $\mfp\Gamma[g_{i}^{-1}]$ is free of rank 1 over
$\Gamma[g_{i}^{-1}]$ for each $i$, and $\left\langle g_1, \dots,
g_{n}\right\rangle = \Gamma$.}

Proving this statement is then a purely algebraic ordeal.

*Proof:* To show the locally free part, note that it suffices to show that
$\mfp$ is a projective $\Gamma$-module. To do so, it will suffice to show that
$\mfp\Gamma_{\mfm}$ is a free $\Gamma_{\mfm}$ module for each maximal ideal
$\mfm \id \Gamma$.

Take any maximal $\mfm \id \Gamma$. If $\mfp \nsubseteq \mfm$, then certainly
$\mfp\Gamma_{\mfm} = \Gamma_{\mfm}$ is a free $\Gamma_{\mfm}$-module. In the
case $\mfp \subseteq \mfm$, note that $\mfp\Gamma_{\mfm}$ is a finitely
generated $\Gamma_{\mfm}$-module. As the latter is local, $\mfm\Gamma_{\mfm}$
is free as desired.

To show that rank 1 part, we need to show that in each $\Gamma_{\mfm}$, the
ideal $\mfp\Gamma_{\mfm}$ is principal. This clearly holds when $\mfp
\nsubseteq \mfm$. Now, consider the case $\mfp \subseteq \mfm$. By
Auslander-Buchsbaum we know that $\Gamma_{\mfm}$ is factorial. Now, consider a
minimal set of generators $p_{1},\dots,p_{n}$ for $\mfp\Gamma_{\mfm}$. These
share no common factors since $\mfp\Gamma_{\mfm}$ is a minimal prime ideal. We
claim that at least one of the $p_{i}$ is irreducible. Suppose without loss of
generality that $p_{1}$ is reducible. Then, $\mfp\Gamma_{\mfm}$ contains at
least one prime factor of $p_{1}$, say $r_{1}$. Clearly $\left\langle r_{1},
p_{2}, \dots,p_{n}\right\rangle \subseteq \mfp\Gamma_{\mfm}$, and the reverse
inclusion holds trivially as well. Hence, we can replace $p_{1}$ with an
irreducible. Now, note that $\left\langle p_{1}\right\rangle$ is a prime
contained in $\mfp\Gamma_{\mfm}$, with $p_{1}$ being nonzero. By minimality of
$\mfp\Gamma_{\mfm}$, this implies that $\mfp\Gamma_{\mfm} = \left\langle
p_{1}\right\rangle$ as desired. $\square$

---

*Proof of proposition, continued:* The line bundle $\Os_{X}(-Y)$ is not exactly
the right bundle we are looking for, but it turns out that its inverse
$\Os_{X}(Y)$ does work. To see this, suppose that $c_{1}(\mcL) = [Y]$. Then, there exists a rational section of $c_{1}$ whose 
