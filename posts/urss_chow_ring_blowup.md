+++
title = "Chow Ring of a Blow-up -- Projective Spaces"
date = Date(2021, 6, 27)
+++
@def tags = ["summer_project", "intersection_theory"]
@def desc = "The Chow ring for the blow-up of projective space at a point"

# Chow Ring of a Blowup -- Projective Spaces

The blow-up is a vital construction in algebraic geometry, giving an almost
canonical method to resolve singularities. In this article, we explain the
construction of blowing up the projective space at a point, and compute its
Chow ring.

We first work with the classical theory of varieties to gain some insight.
Intuitively, by blowing up $\Pb^{n}$ at a point $\xi \in \Pb^{n}$ we are
replacing $\xi$ by the set of all tangential directions, and leave the
remaining points untouched. By applying a projectivity if necessary, we may
assume that $\xi = [1:0:\dots:0]$. Now, identify the subspace $V(X_{0})$ as
$\Pb^{n-1}$. If $p \in \Pb^{n}$ is distinct from $\xi$, then the line joining
$\xi$ and $p$ intersects $\Pb^{n-1}$ exactly once, and we may identify $p$ with
this intersection. We call this the *projection of $p$ from $\xi$*, and denote
this by $\pi_{\xi}(p)$. As $p$ degenerates to $\xi$, however, we find that
depending on how $p$ approaches $\xi$, the limiting value of the projection can
be anything. We may thus think of the blow-up $B$ as a subset of $\Pb^{n}\times
\Pb^{n-1}$, with $$B = \{(p, r) \in \Pb^{n}\times \Pb^{n-1} \mid p=\xi \text{
or } \pi_{\xi}(p) = r\}.$$

To describe $B$ more explicitly, we shall investigate the image of $\pi_{\xi}$.
Let $p = [X_{0}:\dots:X_{n}] \in \Pb^{n}\backslash\{\xi\}$. Points on the line
joining $\xi$ and $p$ has the form $[\lambda + \mu X_{0}: \mu X_{1} : \dots
: \mu X_{n}]$ for some $[\lambda: \mu] \in \Pb^{1}$. A quick computation shows
that the line intersects $\Pb^{n-1}$ when $\lambda = -\mu$, in other words
$\pi_{\xi}(p) = [X_{1}:\dots:X_{n}]$. Choosing $X_{i}$ to be a set of
homogeneous coordinates on $\Pb^{n}$ and $Y_{i}$ a set of homogeneous
coordinates on $\Pb^{n-1}$, we see that points in $B$ satisfies the equations
$X_{i}Y_{j} = X_{j}Y_{i}$ for all $1 \leq i, j \leq n$ (as for why we do not
use $X_{i} = Y_{i}$: note that $X_{i} - Y_{i}$ is not bihomogeneous).

The converse holds as well. Suppose that $p
= [X_{0}:\dots:X_{n}:Y_{1}:\dots:Y_{n}] \in \Pb^{n}\times\Pb^{n-1}$ satisfies
all the quadrics. Without loss of generality, let us assume that $Y_{1} = 1$.
Then, $X_{i}Y_{1} = Y_{i}X_{1}$ for all $1 \leq i \leq n$. Since there exists
some $i$ for which $X_{i} \neq 0$, without loss of generality we may assume
that $X_{i} = 1$. This gives $Y_{1} = X_{1} = 1$. Hence, $X_{i} = X_{i}Y_{1}
= Y_{i}X_{1} = Y_{i}$ for all $1 \leq i \leq n$ and $p \in B$ as desired. This
leads to the following definition.

\defn{Blow-up of a projective space}{The blow-up of $\Pb^{n}$ at $\xi
= [1:0:\dots:0]$ is defined to be the subvariety of $\Pb^{n}\times\Pb^{n-1}$
cut out by the equations $X_{i}Y_{j} = X_{j}Y_{i}$ for $1 \leq i, j \leq n$. To
blow-up at other values of $\xi$, consider a projectivity $\sigma$ taking
$[1:0:\dots:0]$ to $\xi$. Then, $\sigma$ takes $V(X_{0})$ to a hyperplane,
which we also identify with $\Pb^{n-1}$. This induces a projectivity on
$\Pb^{n-1}$, which we call $\tau$. The blow-up with centre $\xi$, written
$\operatorname{Bl}_{\xi} \Pb^{n}$, is the image of $B$ under $\sigma \times
\tau$. The scheme theoretic version of the same construction can be obtained by
applying $\operatorname{Proj}(\cdot)$ to the corresponding homogeneous
coordinate rings.}

Blow-ups are often specified along with a projection $\pi:
\operatorname{Bl}_{\xi} \Pb^{n} \to \Pb^{n}$. Of course, blow-ups of $\Pb^{n}$
with different centres are all isomorphic. This is not true in general for
arbitrary varieties.

We now turn to the problem of describing Chow rings of blow-ups. Given
a blow-up $\pi: \Bl_{\xi} \Pb^{n} \to \Pb^{n}$, there is a special divisor
called the *exceptional divisor* which arises as the scheme-theoretic fibre
$\pi^{-1}(\xi)$. We denote this divisor by $E$. If a hyperplane $\Lambda
\subseteq \Pb^{n}$ does not contain $\xi$, then it would have an isomorphic
preimage under $\pi$. If it does contain $\xi$, then its preimage would be the
union of $E$ with an irreducible subvariety $\tilde{\Lambda}$, which we shall
call the *strict transform* of $\Lambda$. While it is unclear what they do at the moment, they turn out to be useful in the later stages of our computation.
For ease of notation, let $B$ denote the blow-up $\Bl_{\xi} \Pb^{n}$.

As usual, we start with an attempt to stratify $B$. To do this, let us turn to
the standard visualisation of the blow-up of a projective plane, and specialise
to the case $B = \Bl_{\xi}\Pb^{2}$.

\figenv{The blow-up of a projective plane at a point}{/media/blowup.png}{width:35%; margin:auto;}

By an isomorphism if necessary, assume that $\xi = [1:0:0]$. Intuitively, we
wish to mimic the stratification of $\Pb^{2}$. Let $\alpha$ denote the
projection of $B$ onto the second component. Let $\Lambda \subseteq B$ be
pullback of the hyperplane defined by $X_{0} = 0$ under $\pi$, and $\Gamma$ the
pullback of the hyperplane defined by $Y_{1} = 0$ under $\alpha$. Throw in any
point contained in $\Lambda$ and $B$ itself, this gives us what we would like
to be our closed strata (as to why this is an affine stratification: see the
theorem below!).

This process turns out to generalise to other blow-ups. Let $B
= \Bl_{\xi}\Pb^{n}$ as above. Consider a hyperplane $\Lambda^{\prime} \subseteq
\Pb^{n}$, and let $\Lambda$ be its pull back along $\pi$. Now choose a complete
flag of linear subspaces $\Gamma_{0}^{\prime} \subseteq \dots \subseteq
\Gamma_{n-1}^{\prime} = \Pb^{n-1}$, and let $\Gamma_{k+1}$ be the pullback of
$\Gamma_{k}^{\prime}$ under $\alpha$. Let also $\Lambda_{k} = \Lambda \cup
\Gamma_{k+1}$. This choice of stratification allows us to compute the Chow ring
of $B$, as stated by the result below.

\theorem{The Chow ring of $B$}{
With notation as above, $A(B)$ is the free abelian group on generators
$[\Lambda_{k}] = [\lambda_{n-1}]^{n-k}$ for $k = 0,\dots, n-1$ and
$[\Gamma_{k}] = [\Gamma_{n-1}]^{n-k}$ for $k = 1,\dots,n$. The class of the
exceptional divisor $E$ is $[\Lambda_{n-1}] - [\Gamma_{n-1}]$. If we set
$\lambda = [\Lambda_{n-1}]$ and $e = [E]$, then $$ A(B) \cong \Z[\lambda,
e]/\left\langle \lambda e, \lambda^{n} + (-1)^{n}e^{n}\right\rangle $$ as
rings.}

*Proof:*

We break down the computation/proof into a few bite-sized stages.

### 1. Stratification

In this stage, we verify that $\Lambda_{k}$ for $k = 0, \dots, n-1$ and
$\Gamma_{k}$ for $k = 1, \dots, n$ together forms a stratification of $B$. To
do so, consider the following diagram indicating the inclusion relationships of
the strata.

~~~
<iframe class="quiver-embed" src="https://q.uiver.app/?q=WzAsMTAsWzAsMCwiXFxMYW1iZGFfMCJdLFsxLDAsIlxcTGFtYmRhXzEiXSxbMiwwLCJcXGNkb3RzIl0sWzMsMCwiXFxMYW1iZGFfe24tMn0iXSxbNCwwLCJcXExhbWJkYV97bi0xfSJdLFsxLDEsIlxcR2FtbWFfMSJdLFsyLDEsIlxcR2FtbWFfMiJdLFszLDEsIlxcY2RvdHMiXSxbNCwxLCJcXEdhbW1hX3tuLTF9Il0sWzUsMSwiXFxHYW1tYV97bn0iXSxbMCw1LCIiLDAseyJzdHlsZSI6eyJ0YWlsIjp7Im5hbWUiOiJob29rIiwic2lkZSI6InRvcCJ9fX1dLFsxLDYsIiIsMCx7InN0eWxlIjp7InRhaWwiOnsibmFtZSI6Imhvb2siLCJzaWRlIjoidG9wIn19fV0sWzMsOCwiIiwwLHsic3R5bGUiOnsidGFpbCI6eyJuYW1lIjoiaG9vayIsInNpZGUiOiJ0b3AifX19XSxbNCw5LCIiLDAseyJzdHlsZSI6eyJ0YWlsIjp7Im5hbWUiOiJob29rIiwic2lkZSI6InRvcCJ9fX1dLFswLDEsIiIsMSx7InN0eWxlIjp7InRhaWwiOnsibmFtZSI6Imhvb2siLCJzaWRlIjoidG9wIn19fV0sWzEsMiwiIiwxLHsic3R5bGUiOnsidGFpbCI6eyJuYW1lIjoiaG9vayIsInNpZGUiOiJ0b3AifX19XSxbMiwzLCIiLDEseyJzdHlsZSI6eyJ0YWlsIjp7Im5hbWUiOiJob29rIiwic2lkZSI6InRvcCJ9fX1dLFszLDQsIiIsMSx7InN0eWxlIjp7InRhaWwiOnsibmFtZSI6Imhvb2siLCJzaWRlIjoidG9wIn19fV0sWzUsNiwiIiwxLHsic3R5bGUiOnsidGFpbCI6eyJuYW1lIjoiaG9vayIsInNpZGUiOiJ0b3AifX19XSxbNiw3LCIiLDEseyJzdHlsZSI6eyJ0YWlsIjp7Im5hbWUiOiJob29rIiwic2lkZSI6InRvcCJ9fX1dLFs3LDgsIiIsMSx7InN0eWxlIjp7InRhaWwiOnsibmFtZSI6Imhvb2siLCJzaWRlIjoidG9wIn19fV0sWzgsOSwiIiwxLHsic3R5bGUiOnsidGFpbCI6eyJuYW1lIjoiaG9vayIsInNpZGUiOiJ0b3AifX19XV0=&embed" width="818" height="304" style="border-radius: 8px; border: none;"></iframe>
~~~

Clearly, pairs of open strata of the form $(\Gamma_{i}^{\circ},
\Gamma_{j}^{\circ})$ or $(\Lambda_{i}^{\circ}, \Lambda_{i}^{\circ})$ has empty
intersection. For tuples of the form $(\Gamma_{i}^{\circ},
\Lambda_{j}^{\circ})$, when $i > j$ clearly the two strata are disjoint. If $j
\geq i$, then $\Lambda_{j} \cap \Gamma_{i} = \Lambda \cap \Gamma_{j+1} \cap
\Gamma_{i} = \Lambda \cap \Gamma_{i} = \Lambda_{i-1}$. Hence,
$\Gamma_{i}^{\circ} \cap \Lambda_{j}^{\circ} \subseteq \Gamma_{i} \cap
(\Lambda_{j} \backslash \Lambda_{i-1}) = \varnothing$. This does the trick.

### 2. Affine Stratification

Now, we verify that each open stratum is indeed isomorphic to $\A^{k}$ for some
$k$. That the strata $\Lambda_{k}^{\circ}$ for $k = 0, \dots, n-1$ are affine
is clear: $\Lambda_{k}$ is the preimage of $\Gamma_{k}^{\prime}$ under the
isomorphism $\alpha\vert_{\Lambda} \Lambda \to \Pb^{n-1}$.

For $\Gamma_{k}^{\circ}$, by an appropriate choice of coordinates suppose that
$\xi = [1:0:\dots:0]$ and $\Lambda^{\prime} \subseteq \Pb^{n}$ is given by
$X_{0} = 0$. Suppose also that $\Gamma^{\prime}_{i-1}$ is given by $Y_{1}
= \dots = Y_{n-i} = 0$. Then, we have $\Gamma_{k}^{\circ}
= \Gamma_{k}\backslash(\Gamma_{k-1} \cup \Lambda_{k-1})
= \alpha^{-1}(\Gamma_{k-1}^{\prime}\backslash\Gamma_{k-2}^{\prime})\cap(B\backslash\Lambda)$.
Writing this out explicitly, we have $$\Gamma_{k}^{\circ} = \left\{([1: 0:
\dots : 0 : \lambda : \lambda y_{n-k+2} : \dots : \lambda y_{n} ], [0: \dots
: 0 : 1 : y_{n-k+2} : \dots : y_{n}])\right\}.$$ The map sending such a point
to $(\lambda, y_{n-k+2}, \dots, y_{n}) \in \A^{k}$ gives the desired
isomorphism.

Now, write $\lambda_{k} = [\Lambda_{k}]$ and $\gamma_{k} = [\Gamma_{k}]$.
Standard results on affine stratification shows that they generate $A(B)$.

### 3. Intersecting Generators

We now turn to how the classes of the strata behave under the intersection
product. As $\Lambda_{k}$ is the preimage of a $k$-plane in $\Pb^{n}$ not
containing $\xi$ ($\Lambda_{k}$ projects to such a plane under $\pi$), and any
two such $k$-planes are rationally equivalent, it follows that the pullback of
their classes are all $\lambda_{k}$.

Similarly, $\Gamma_{k}$ projects via $\pi$ to a $k$-plane in $\Pb^{n}$
containing $\xi$. A simple computation shows that the preimage of this plane
under $\pi$ is the union of $\Gamma_{k}$ with $E$, so $\Gamma_{k}$ is the
strict transformation of a $k$-plane in $\Pb^{n}$ containing $\xi$. As any two
such $k$-planes are rationally equivalent, it follows that the classes of their
pullback are all equal to $\gamma_{k}$.

This makes it possible to compute intersection products of the generators. For
instance, a general $k$-plane and a general $l$-plane intersects at
a $(k+l-n)$-plane transversally (when $k+l\geq n$), so $$\lambda_{k}\lambda_{l}
= \lambda_{k+l-n}.$$ Note that if we let $\lambda = \lambda_{n-1}$, then
$\lambda^{n-k} =\lambda_{k}$ for all $k = 0, 1, \dots, n-1$. Similarly,
a general $k$-plane containing $\xi$ and a general $l$-plane not containing $p$
intersects transversally at a $(k+l-n)$-plane not containing $p$, so
$$\gamma_{k}\lambda_{l} = \lambda_{k+l-n}$$ for all $k+l \geq n$. Likewise,
$\gamma_{k}\gamma_{l} = \gamma_{k+l-n}$ for all $k+l \geq n+1$. Note that the
$n+1$ is necessary here -- if $k+l = n$, then we expect two such planes to
intersect at a point, in this case $\xi$. There strict transform therefore will
not intersect.

### 4. Linear Independence of Generators

It is now possible to show that the generators we found are indeed linearly
independent. Consider a linear combination $\sum_{i} a_{i}g_{i}$ of the
generators, with each $a_{i} \in \Z_{\neq 0}$. Each $g_{i}$ is of the form
$\lambda_{k}$ or $\gamma_{k}$ for some $k \in \Z_{\geq 0}$. Consider the
smallest $k$ occurring amongst these subscripts, and multiply both sides by
$\lambda_{n-k}$. Then, all terms with subscript greater than $k$ gets killed,
so only terms with subscript equal to $k$ remains. Letting $a$ be the
coefficient of $\lambda_{k}$ and $b$ that of $\gamma_{k}$ in the sum, this
shows that $a + b = 0$. Now, multiplying both sides by $\gamma_{n-k}$ kills all
terms with subscript greater than $k$, as well as $\gamma_{k}$. This shows that
$b = 0$, thus $a = 0$ as well, a contradiction.

Hence, the generators
$\lambda_{0},\dots,\lambda_{n-1},\gamma_{1},\dots,\gamma_{n}$ in fact generates
$A(B)$ freely.

### 5. Class of the Exceptional Divisor

We now verify that $[E] = \lambda_{n-1} - \gamma_{n-1}$. Indeed, as
$\Lambda_{n-1}^{\prime}$ is rationally equivalent to a hyperplane $\Sigma
\subseteq \Pb^{n}$ containing $\xi$, it follows that $\lambda_{n-1}
= [\pi^{-1}(\Sigma)]$. Now, $\pi^{-1}(\Sigma)$ is the union of $E$ and its
strict transform $D$ -- a irreducible divisor. Now, note that $D$ projects to
a hyperplane via $\alpha$, so it is contained in the pullback of a hyperplane.
Denote this pullback by $\Gamma$. We know that $\Gamma$ is irreducible, as it
is a $\Pb^{1}$-bundle over $\Pb^{n-1}$ and thus locally irreducible. As
$\Gamma$ and $D$ has the same dimension, we conclude that $\Gamma = D$. Thus,
$\lambda_{n-1} = [\pi^{-1}(\Sigma)] = [E] + [D] = e + [\Gamma_{n-1}]
= e + \gamma_{n-1}$. In other words, $e = [E] = \lambda_{n-1} - \gamma_{n-1}$.

### 6. Complete Determination of $A(B)$

As in part 3, write $\lambda = \lambda_{n-1}$. It is clear that $\lambda$ and
$e$ generates $A(B)$ as a ring. To derive the first relationship, note that
$\Lambda_{n-1} \cap E = \varnothing$ (by definition), so $\lambda e = 0$. For
the second relationship, we have $$0 = \gamma_{n-1}^{n} = (\lambda - e)^{n}
= \lambda^{n}  + (-1)^{n}e^{n}$$ as all terms containing $\lambda e$ vanish by
the first relationship.

Hence, $A(B)$ is a homomorphic image of $A^{\prime} = \Z[\lambda,
e]/\left\langle \lambda e, \lambda^{n} + (-1)^{n}e^{n}\right\rangle$. To show
equality, note that $A^{\prime}$ is a graded ring. Every degree $m$ element of
$A^{\prime}$ is a $\Z$-linear combination of $e^{m}$ and $\lambda^{m}$. The
degree $0$ component and degree $n$ component of $A^{\prime}$ and $A(B)$ are
clearly isomorphic via the quotient map. For each $0 < m < n$, the degree $m$
component of $A^{\prime}$ has degree 2, which is equal to that of $A(B)$. Thus,
the quotient map must have been an isomorphism, which is what we wanted.
$\square$

------

Now that $A(B)$ has been determined, we can smell some peculiarities of this
object. To start off with, we have $e = \lambda - \gamma_{n-1}$, so somehow $E$
contains a pole! I have a gut feeling that this has something to do with Chern
classes, but I can probably only make this precise after a while.

The second peculiarity is that we have $e^{2} = e(\lambda - \gamma_{n-1}) =
-\gamma_{n-1}e = -[\Gamma_{n-1} \cap E]$, which seems to suggest that if we try
to add differential information to $E$, then we will get something that looks
*purely* like a pole. Once again, I have a feeling that this might have
something to do with Chern classes. I will try to verify this once I can make
sense of Chern classes in general.
