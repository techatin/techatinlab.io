+++
title = "Hartshorne Exercise II.3.1"
date = Date(2021, 3, 27)
+++
@def tags = ["hartshorne", "easy"]
@def desc = "Ex II.3.1: being locally of finite type is affine local on the target."

# Hartshorne Exercise II.3.1

**Ex II.3.1** Show that a morphism $f: X \to Y$ is locally of finite type if
and only if for _every_ affine open subset $V = \Spec B \subseteq Y$,
$f^{-1}(V)$ can be covered by open affine subsets $U_{j} = \Spec A_{j}$, where
each $A_{j}$ is a finitely generated $B$-algebra.

### Solution

This is a somewhat straightforward application of affine communication lemma,
which we shall prove below so we feel entitled to use it later.

\lemma{cover by principal open sets}{Let $X$ be a scheme, and $\Spec A, \Spec
B$ be two open subschemes of $X$. Then, the intersection $\Spec A \cap \Spec B$
can be covered by subsets principally open in both $\Spec A$ and $\Spec B$.}

*Proof:* Let $p \in \Spec A \cap \Spec B$. Then, there exists some $f \in A$
such that $p \in \Spec A_{f} \subseteq \Spec A \cap \Spec B$. As $\Spec A_{f}$
is open in $\Spec B$, there exists some $g \in B$ such that $\Spec B_{g}
\subseteq \Spec A_{f}$. As $\Spec A_{f}$ is open in $\Spec B$, we know that $g \in \Os_{X}(\Spec B)$ can be restricted to $g^{\prime} \in \Os_{X}(\Spec A_{f})$.

Now, note that $g$ vanishes at a point in $\Spec A_{f}$ if and only if
$g^{\prime}$ does, so
\begin{align}
\Spec B_{g} &= \Spec A_{f} \backslash \{\mathfrak{p} \mid g^{\prime} \notin \mathfrak{p}\} \\
&= \Spec A_{fg^{\prime}}
\end{align}
is an open neighbourhood of $p$ that is principally open in $\Spec A$ and $\Spec B$ simultaneously, as desired. $\square$

This puts us in a good position to prove the affine communication lemma.

\lemma{affine communication lemma}{Let $P$ be a property satisfied by some open
affine subschemes of a scheme $X$, such that:
- if an affine open subscheme $\Spec A \hookrightarrow X$ has $P$ and $f \in
  A$, then $\Spec A_{f} \hookrightarrow$ also has $P$, and
- if $\left\langle f_{1}, \dots, f_{n}\right\rangle = A$ and each $\Spec
  A_{f_{i}} \hookrightarrow X$ has $P$, then $\Spec A \hookrightarrow X$ also
  has $P$.

Such a property is said to be an affine local property. If $X$ can be covered
by affine open subsets $\cup_{i \in I} \Spec A_{i}$, with each $A_{i}$ having
property $P$, then every affine open subset of $X$ has property $P$.}

*Proof:* Let $\Spec A \subseteq X$ be an open subscheme. We know that for each
$i \in I$, the open subscheme $\Spec A \cap \Spec A_{i}$ can be covered by
subsets principally open in both. That is, $\Spec A \cap \Spec A_{i}$ can be
covered by open subsets of the form $\Spec A_{g}$, with $\Spec A_{g}$ having
property $P$. By quasicompactness of $\Spec A$, finitely many of these, say
$\Spec A_{g_{1}}, \dots, \Spec A_{g_{n}}$, cover $\Spec A$, so $A
= \left\langle g_{1}, \dots, g_{n}\right\rangle$. By the second property of
$P$, we know that $\Spec A$ has $P$ as desired. $\square$

The exercise thus boils down to showing that being locally of finite type is
affine local.

\prop{being locally of finite type is affine local}{Let $X, Y$ be schemes. The
property that $f: f^{-1}(\Spec A) \to \Spec A \subseteq Y$ is locally of finite
type is affine local on $Y$.}

*Proof:* If $f: X \to \Spec A$ is locally of finite type, then $X
= f^{-1}(\Spec A)$ can be covered by affine open subsets $\Spec B_{i}$, where
each $B_{i}$ is a finitely generated $A$-algebra. Let $r \in A$. Then,
$f^{-1}(\Spec A_{r}) \cap \Spec B_{i} = \Spec B_{i}[f^{\sharp}(r)^{-1}] = \Spec
B^{\prime}_{i}$, where each $B^{\prime}_{i}$ is a finitely generated
$A_{r}$-algebra.

Now, suppose that $A = \left\langle r_{1}, \dots, r_{n}\right\rangle$, with
each $f: f^{-1}(\Spec A_{r_{i}}) \to \Spec A_{r_{i}}$ being locally of finite
type. That is, $f^{-1}(\Spec A_{r_{i}})$ can be covered by affine open subsets
$\Spec B_{i}$ where each $B_{i}$ is a finitely generated $A_{r_{i}}$ algebra,
and thus a finitely generated $A$-algebra. Hence, $f^{-1}(\Spec A)$ yields an
open cover by the spectra of finitely generated $A$-algebras.

This shows that being locally of finite type is indeed affine local on the
target, as desired. $\square$

The proposition and affine communication lemma together immediately shows the
only if direction, while the if direction is trivial.
