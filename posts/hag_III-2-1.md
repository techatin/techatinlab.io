+++
title = "Hartshorne Exercise III.2.1"
date = Date(2021, 07, 23)
+++
@def desc = "Cohomology of a certain sheaf"
@def tags = ["hartshorne", "cohomology"]

# Hartshorne Exercise III.2.1

**Exercise III.2.1:** (a) Let $X = \A^{1}_{k}$ be the affine line over an
infinite field $k$. Let $P, Q$ be distinct closed points of $X$, and let $U
= X\backslash\{P, Q\}$. Show that $H^{1}(X, \Z_{U}) \neq 0$.

(b) More generally, let $Y \subseteq X = \A^{n}_{k}$ be the union of $n+1$ hyperplanes in suitably general position, and let $U = X\backslash Y$. Show that $H^{n}(X, \Z_{U}) \neq 0$. Thus the result of 2.7 is best possible.

### Solution

**(a)** Recall that the sheaf $\Z_{U}$ is the extension of the constant sheaf
$\overline{\Z}\vert_{U}$ by 0, i.e. the sheaf associated to the presheaf
$\mathscr{F}$ where $\mathscr{F}(V) = \overline{\Z}(V)$ if $V \subseteq U$ and
$0$ otherwise. Consider the following exact sequence of sheaves $$0 \rightarrow
\Z_{U} \rightarrow \overline{\Z} \rightarrow i_{\ast}(\overline{\Z})
\rightarrow 0, $$ where $i$ is the inclusion $\{P, Q\} \subseteq X$. To
determine $H^{1}(X, \Z_{U})$, we would need to determine the global sections of
the three nonzero terms.

The global section of the last two sheaves are well-known, so we only need to
determine $\Gamma(X, \Z_{U})$. As $k$ is infinite, so is $\A_{k}^{1}$. Given
any point $R \in \A_{k}^{1}$, the stalk of $\Z_{U}$ at $R$ is $0$ is $R = P$ or
$Q$, and $\Z$ otherwise. The global section $\Gamma(X, \Z_{U})$ consists of all
tuples of compatible germs $(s_{x})_{x \in X}$. Given a point $x \in
\A_{k}^{1}$, any of its (connected) neighbourhood intersects non-trivially with
any (connected) neighbourhood of $P$, it follows that the germs must be
identically 0, so $\Gamma(X, \Z_{U}) = 0$.

Thus, after applying $\Gamma(X, \cdot)$, the short exact sequence becomes $$0
\rightarrow 0 \rightarrow \Z \rightarrow \Z^{\oplus 2} \rightarrow H^{1}(X,
\Z_{U}) \rightarrow H^{1}(X, \overline{\Z}) = 0 \rightarrow \dots$$ By rank
counting, it follows that $H^{1}(X, \Z_{U})$ has rank $1$ and is thus nonzero.

**(b)** 
