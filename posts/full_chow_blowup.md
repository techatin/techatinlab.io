+++
title = "Chow Ring of a Blowup"
date = Date(2022, 9, 5)
+++
@def tags = ["intersection_theory"]
@def desc = "We show how to compute the Chow ring of a nonsingular variety blown up along a smooth subvariety."

~~~
<style>
body {
    overflow-x: visible;
}
</style>
<script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
<script type="text/javascript">
function doIframe() {
    var $iframes = $("iframe.autoHeight"); 
    $iframes.each(function() {
        var iframe = this;
        setHeight(iframe);
        $(".pc", iframe.contentWindow.document).addClass("opened");
        console.log($(".pc", iframe.contentWindow.document));
    });
}


function setHeight(e) {
  e.height = e.contentWindow.document.getElementById("page-container").scrollHeight + 35;
  console.log(e.height);
}

</script>
<div style="position: absolute; width: 100vw; left: calc(-50vw + 50%); margin: auto;">
<iframe class="autoHeight" src="/media/full_chow_blowup/index.html" style="margin:auto; width: 100vw; border:0px;" onload="doIframe();" seamless="seamless"></iframe>
</div>

~~~
