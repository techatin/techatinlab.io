+++
title = "Hartshorne Exercise II.3.13"
date = Date(2021, 4, 4)
+++
@def tags = ["hartshorne"]
@def desc = "Ex II.3.13: Properties of morphisms of finite type"

# Hartshorne Exercise II.3.13

**Ex II.3.13 a)** A closed immersion is a morphism of finite type.

### Proof

Let $f: X \to Y$ be a closed immersion and $\Spec A$ an affine open subset of
$Y$. Put $X^{\prime} = f^{-1}(\Spec A)$, so $f|_{X^{\prime}}: X^{\prime} \to
\Spec A$ is closed.

By II.3.11 b), we know that $X^{\prime} = \Spec B$ for some ring $B$. As
$f^{\sharp}: A \to B$ is a surjection of rings, we know that $B = A/\mfa$ for
some $\mfa \id A$, so $B$ is a finitely generated algebra over $A$
as desired.

-----

**Ex II.3.13 b)** A quasicompact open immersion is of finite type.

### Proof

Let $f: X \to Y$ be a quasicompact open immersion, and $\Spec A$ an open
subscheme of $Y$. Put $U = f^{-1}(\Spec A)$, so $U$ is quasicompact.

Let $V = f(U)$, so $V$ is an open subscheme of $\Spec A$. Let $\mathfrak{A}
= \left\{\Spec A_{f_{i}}\right\}_{i \in I}$ be an affine open cover of $V$.
Since $f$ is an open immersion, we know that $f^{-1}(\Spec A_{f_{i}}) \cong
\Spec A_{f_{i}}$ for each $i$, with $A_{f_{i}}$ being a finitely generated
algebra over $A$.

As the $\Spec A_{f_{i}}$ cover $V$, $f^{-1}(\Spec A_{f_{i}})$ cover $U$. As $U$
is quasicompact, $U$ admits a finite open cover by spectra of algebras finitely
generated over $A$, as desired.

Note that this becomes obviously wrong once we drop the requirement of
quasicompactness. For instance, consider the immersion of $U = \Spec k[x_{1},
x_{2}, \dots] \backslash V(\left\langle x_{1}, x_{2}, \dots\right\rangle )$ in
$\Spec k[x_{1}, x_{2}, \dots]$. In this case, $U$ does not even admit a finite
affine cover.

---

**Ex II.3.13 c)** A composition of two morphisms of finite type is of finite
type.

### Proof

Let $f: X \to Y$ and $g: Y \to Z$ be morphisms of finite type.

Let $\Spec A$ be an open affine subscheme of $Z$. As $g$ is of finite type,
$g^{-1}(\Spec A)$ is covered by finitely many spectra of finitely generated
$A$-algebras, say $\left\{\Spec B_{i}\right\}_{i=1}^{n}$. As $f$ is of finite
type, $f^{-1}(\Spec B_{i})$ is covered by finitely many spectra of finitely
generated $B_{i}$-algebras, say $\left\{\Spec C_{ij}\right\}_{j=1}^{n_{i}}$. As
$C_{ij}$ is finitely generated over $B_{i}$ and $B_{i}$ is finitely generated
over $A$, each $C_{ij}$ is finitely generated over $A$. As the $\Spec C_{ij}$
cover $(g\circ f)^{-1}(\Spec A)$, it yields a finite cover by spectra of
finitely generated $A$-algebra, as desired.

---

**Ex II.3.13 d)** Morphisms of finite type are stable under base extensions.

### Proof

Let $f: X \to Y$ be a morphism of finite type and $g: X^{\prime} \to Y$
a morphism of schemes. Consider the following pullback square

~~~
<!-- https://q.uiver.app/?q=WzAsNCxbMSwxLCJZIl0sWzEsMCwiWCJdLFswLDEsIlheXFxwcmltZSJdLFswLDAsIlhcXHRpbWVzX1lYXlxccHJpbWUiXSxbMywyLCJwXzIiLDJdLFsyLDAsImciLDJdLFszLDEsInBfMSJdLFsxLDAsImYiXV0= -->
<iframe class="quiver-embed" src="https://q.uiver.app/?q=WzAsNCxbMSwxLCJZIl0sWzEsMCwiWCJdLFswLDEsIlheXFxwcmltZSJdLFswLDAsIlhcXHRpbWVzX1lYXlxccHJpbWUiXSxbMywyLCJwXzIiLDJdLFsyLDAsImciLDJdLFszLDEsInBfMSJdLFsxLDAsImYiXV0=&embed" width="363" height="304" style="border-radius: 8px; border: none;"></iframe>
~~~

Let $\left\{\Spec A_{i}\right\}_{i \in I}$ be an affine open cover of $Y$. For
each $i \in I$, we can cover $g^{-1}(\Spec A_{i})$ by affine opens
$\left\{\Spec B_{ij}\right\}_{j \in J}$. We also know that $f^{-1}(\Spec
A_{i})$ can be covered by finitely many spectra of finitely generated
$A_{i}$-algebras, say $\left\{\Spec C_{ik}\right\}_{k=1}^{n_{i}}$.

Now, by a similar argument as what we saw in the construction of fibred
products, we know that $p_{2}^{-1}(\Spec B_{ij}) \cong X \times_{\Spec A_{i}}
\Spec B_{ij}$ can be covered by $\Spec C_{ik} \times_{\Spec A_{i}} \Spec B_{ij}
\cong \Spec(C_{ik} \otimes_{A_{i}} B_{ij})$, where $k$ runs from $1$ to
$n_{i}$. As $C_{ik}$ is a finitely generated $A_{i}$-algebra, we know that
$C_{ik} \otimes_{A_{i}} B_{ij}$ is a finitely generated $B_{ij}$-algebra, so
$X^{\prime}$ yields an open affine cover $\left\{\Spec B_{ij}\right\}_{i \in I,
j \in J}$ such that each $p_{2}^{-1}(\Spec B_{ij})$ can be covered by finitely
many spectra of finitely generated $B_{ij}$ algebras. Thus, $p_{2}$ is of
finite type as desired.

Hence, morphisms of finite type are stable under base extension.

---

**Ex II.3.13 e)** If $X$ and $Y$ are schemes of finite type over $S$, then
$X\times_{S} Y$ is of finite type over $S$.

### Proof

Let $f: X \to S$ and $g: Y \to S$ be the structure morphisms of $X$ and $Y$
respectively. Write $p_{1}: X \times_{S} Y \to X$ and $p_{2}: X \times_{S}
Y \to Y$ be the relevant projection maps. We wish to show that $h = f\circ
p_{1} = g\circ p_{2}$ is a morphism of finite type.

Let $\Spec A$ be an affine open subscheme of $S$. Cover $f^{-1}(\Spec A)$ by
$\left\{\Spec B_{i}\right\}_{i = 1}^{n}$ and $g^{-1}(\Spec A)$ by
$\left\{\Spec C_{j}\right\}_{j = 1}^{m}$.

Now, $h^{-1}(\Spec A) = f^{-1}(\Spec A) \times_{S} g^{-1}(\Spec A)$ is covered
by $\left\{\Spec(B_{i} \otimes_{A} C_{j})\right\}_{i, j}$. As both $B_{i}$ and
$C_{j}$ are finitely generated $A$-algebras, we know that $B_{i} \otimes C_{j}$
is a finitely generated $A$-algebra as well.

---

**Ex II.3.13 f)** If $X \xrightarrow{f} Y \xrightarrow{g} Z$ are two morphisms
such that $f$ is quasicompact and $g \circ f$ is of finite type. Then, $f$ is
of finite type.

### Proof

Cover $Z$ by $\Spec A_{i}, i \in I$ and $g^{-1}(\Spec A_{i})$ by $\Spec B_{ij},
j \in J$. Cover $(g\circ f)^{-1}(\Spec A_{i})$ by $C_{k}, k \in K$. Here, $I,
J$ and $K$ are all finite, with the $B_{ij}$ and $C_{k}$ being finitely
generated $A_{i}$-algebras.

Now, consider $f^{-1}(\Spec B_{ij})$. This can be covered by open subschemes of
the form $\Spec C_{k_{\alpha}}[f_{\alpha}^{-1}]$. As $f$ is quasiprojective, we
may assume that the $\alpha$ come from a finite set. As
$C_{k_{\alpha}}[f_{\alpha}^{-1}]$ is a finitely generated $A$-algebra and also
a $B_{ij}$-algebra, its generators over $A$ generates it over $B_{ij}$, so it
is also a finitely generated $B_{ij}$-algebra. Thus, $Y$ admits an affine open
cover $\Spec B_{ij}$, such that each $f^{-1}(\Spec B_{ij})$ admits a finite
open cover by spectra of finitely generated $B_{ij}$-algebras, thus $f$ is of
finite type as desired.

---

**Ex II.3.13 g)** Let $f: X \to Y$ be a morphism of finite type. If $Y$
noetherian, then so is $X$.

### Proof

Let $\left\{\Spec A_{i}\right\}_{i=1}^{n}$ be an open affine cover of $Y$, with
each $A_{i}$ being noetherian. As $f$ is of finite type, it is quasicompact, so
each $f^{-1}(\Spec A_{i})$ admits a finite cover by spectra of finitely
generated $A_{i}$ algebras, thus it admits a finite cover by spectra of
noetherian rings. Thus, $X$ admits a finite cover by spectra of noetherian
rings, so $X$ is noetherian as desired.
