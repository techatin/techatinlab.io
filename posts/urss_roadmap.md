+++
title = "Summer Research Project Roadmap"
date = Date(2021, 6, 22)
+++
@def tags = ["summer_project"]
@def desc = "A brief outline of my summer research project plan."

# Summer Research Project Roadmap

In this project, titled 'Lines on Quintic Threefolds', I aim to understand the
algebro-geometric ideas behind the classical result that a general quintic
threefold in $\Pb^{4}$ contains exactly 2875 lines. Below, I will outline the
argument, filling in the gaps with subsequent blog posts which also function as
weekly progress documentations.

The idea is that attempting to see which line is incident to a given quintic
threefold (or hypersurface in general) is hard, and more so is attempting to
enumerate them. Hence, we would really want to change our perspective and
consider instead the space $\G(2, 5)$ of lines in $\Pb^{4}$, and enumerate the
zero locus of a general quintic form. Denoting by $\mathcal{S}$ the
tautological bundle over $\G(2, 5)$, a quintic form $g$ corresponds to a global
section $\sigma_{g}$ of the symmetric power $\Sym^{5}\mathcal{S}^{\ast}$.

While this does not seem like a reduction in any way, we have in fact
simplified the problem as there are well-known tools for addressing problems of
this form. Write $X = V(g)$ and define the Fano scheme $F_{1}(X)$ as the zero
locus of $\sigma_{g}$. Using standard tools in intersection theory, we know
that the collection of lines contained in $X$ is finite if $F_{1}(X)$ has the
expected codimension of $\binom{5}{2} = 10$, and the lines are all counted with
multiplicity 1 if $F_{1}(X)$ is reduced.

At this point, it is perhaps worth noting that what we considered above is
really a nice case in the general theory of linear subspaces on hypersurfaces.
In general, if we consider the problem of enumerating $k$-planes on degree $d$
hypersurfaces of $\Pb^{n}$, then denoting by $\mathcal{S}$ the tautological
bundle on $\G(k+1, n+1)$ as before the expected codimension of $F_{k}(X)$ would
be $\rank(\Sym^{d} \mathcal{S}^{\ast}) = \binom{k+d}{k}$. This number may not
be the dimension of $\G(k+1, n+1)$, in which case we do not expect $F_{k}(X)$
to be a finite collection of points. Nonetheless, this already gives some
indication as to whether we should expect infinitely many $k$-planes on $X$, or
none at all.

Returning to our special case, a result (Prop. 6.4 [Eis]) tells us that when
the codimension is as expected, the class of $F_{1}(X)$ in the Chow ring
$A(\G(2, 5))$ coincides with the top Chern class of $\Sym^{5}
\mathcal{S}^{\ast}$. The Chern class of a vector bundle, being a well known
invariant for years, is significantly more accessible. Taking the degree of the
top Chern class gives the number of lines incident to $X$, counted with
multiplicity. A bit more work will then show that in the general case, all
lines are counted with multiplicity 1.

With the sketch done, below I will outline the work required to fill in the
technical details of this argument.

- A general understanding of Chow rings (partially done, I would like to work out a few examples in the upcoming period of time).

    - Worked example: [Chow ring of a blow-up](/posts/urss_chow_ring_blowup/).

- Computation of Chow rings for Grassmannians.

- Definition of Chern classes.

- Methods to compute Chern classes, including the splitting principle, Whitney formula, etc.

- Interaction between Chern classes and Fano schemes, or Hilbert schemes in general.

These cover roughly Chapters 1-6 from *3264 & All That: Intersection Theory in
Algebraic Geometry*([Eis]) by Eisenbud and Harris. Here's to a fruitful summer
full of algebraic geometry!


