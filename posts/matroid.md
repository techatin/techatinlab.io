+++
title = "A Myriad of Matroids"
date = Date(2022, 9, 2)
+++
@def tags = ["combinatorics", "misc"]
@def desc = "Many equivalent formulations for matroids, and even more examples"
@def maxtoclevel = 2

# A Myriad of Matroids

A matroid is a combinatorial gadget that keeps track of dependency
informations. I became interested in these objects because they play an
important role in the theory of hyperplane arrangements. Specifically, the keep
track of combinatorial information that allows us to construct
compactifications of complements of hyperplane arrangements, and to describe
their intersection theory.

Since the moduli spaces $\overline{M}_{0, n}$ naturally arise as one of these
compactifications, we can extract birational information of these spaces using
matroids. This is the goal of my thesis project, but I am currently pursuing
a much less ambitious goal: I want to use the remainder of my holidays to get
over the combinatorial over head, and work out from first principle the Chow
rings of de Concini--Procesi (dCP) wonderful compactifications of hyperplane
arrangements.

This article is the first step in the series, and aims to provide (equivalent)
definitions of a matroid, and a lot of examples.

\toc

## Definition in terms of independence

The first and most orthodox formulation of matroids revolves around the idea
that a matroid keeps track of when a subset of a ground set $E$ contains
independent elements.

\defn{I}{ An $\mathrm{I}$-matroid is a tuple $(E, \mc{I})$, where $E$ is a set
(usually assumed to be finite), called the ground set, and $\mc{I} \subset 2^E$
is a collection of subsets of $E$, satisfying the following:

1. $\varnothing \in \mc{I}$ (the empty set is independent),
2. $\mc{I}$ is closed under taking subsets (subset of an independent set is independent),
3. For any $I, J \in \mc{I}$ with $|I| < |J|$, we can find some $s \in J\setminus I$ such that $I \cup \{s\} \in \mc{I}$ }

Item I.2 and I.3 are often referred to as the *hereditary* and *augmentation*
properties, respectively. The augmentation property might be slightly unintuitive, but it can be easily explained using the following example.

\example{ **[linear matroid]** Let $V/k$ be a vector field, and $E$ a finite
subset of $V$. Let $\mc{I}$ be the collection of linearly independent subsets
of $E$. Then, $(E, \mc{I})$ is a matroid, called the *linear matroid* on $E$.
Indeed, I.1 and I.2 holds trivially. Property I.3 holds because for all $I,
J \in \mc{I}$, we know that $|I| < |J|$ implies that $\mathrm{Span}(I)
\nsubseteq \mathrm{Spac}(J)$, thus we can always add a new vector to $I$
without breaking its linear dependence. In other words, the augmentation
property is just a combinatorial manifestation of sifting.}

Of course, we can streamline the data we are working with by looking at only
maximal independent sets in $M$, whose collection we denote by $\mc{B}(M)$. In
this case, a subset of $E$ is independent if and only if it is contained in an
element of $\mc{B}(M)$. In the setting of linear matroids, $\mc{B}(M)$ consists
precisely of subsets of $E$ which form a basis for $\mathrm{Span}(E)$. This is
also why members of $\mc{B}(M)$ are called bases of $M$. It turns out that we
can also characterise a matroid in terms of properties its bases have to
satisfy.

## Definition in terms of base

\defn{B}{ A $\mathrm{B}$-matroid is a tuple $(E, \mc{B})$, where $E$ is
a set and $\mc{B} \subseteq 2^{E}$ is a collection of subsets such that 

1. $\mc{B} \neq \varnothing$,
1. For any $B_{1}, B_{2} \in \mc{B}$ and $x \in B_{1} \setminus B_{2}$, there exists some $y \in B_{2} \setminus B_{1}$ such that $(B_{1}\setminus \{x\}) \cup \{y\} \in \mc{B}$.}

Item B.2 is also called the *base exchange property*. Once again, in the case
of a linear matroid this can be seen as a combinatorial manifestation of
sifting.

The proof of equivalence will be a simple ad-hoc argument that does the
'obvious' thing.

### A $\mathrm{B}$-matroid gives us an $\mathrm{I}$-matroid

Given a $\mathrm{B}$-matroid $(E, \mc{B})$, let $\mc{I} = \cup_{B \in \mc{B}}
[\varnothing, B]$ be the downward closure of $\mc{B}$ in $2^{E}$. Consider the
tuple $(E, \mc{I})$, which we will show to be an $\mathrm{I}$-matroid. Now,
items I.1 and I.2 clearly holds.

For item I.3, we would need a stronger result about the cardinality of the
bases.

\lemma{equicardinality of the bases}{Given two bases $B_{1}, B_{2}$ of
a $\mathrm{B}$-matroid $(E, \mc{B})$, we have $|B_{1}| = |B_{2}|$.}

*Proof:* Suppose the contrary that $|B_{1}| < |B_{2}|$. By a repeated
application of the base exchange property, we can replace all elements of
$B_{1}$ with that of $B_{2}$, so we may assume that $B_{1} \subsetneq B_{2}$. This immediately contradicts the base exchange property -- indeed, we can find some element $x \in B_{2} \setminus B_{1}$, but no element $y \in B_{1} \setminus B_{2}$. $\quad\square$

Returning to our verification of property I.3, take any $I, J \in \mc{I}$ with
$|I| < |J|$. If $I$ and $J$ are contained in a common base, then we are clearly
done. Else, consider distinct bases $B_{1}, B_{2} \in \mc{B}$ such that $I
\subseteq B_{1}$ and $J \subseteq B_{2}$. If $B_{1} \setminus I \subseteq
B_{2}$, then since $|B_{1}\setminus I| > |B_{2} \setminus J|$, we know that we
can find some $z \in J \cap (B_{1}\setminus I) \subseteq J\setminus I$. By
definition of $\mc{I}$, we then know that $I \cap \{z\} \in \mc{I}$ as desired.
Else, by the bases exchange property we can take an element of $B_{1} \setminus
(I \cup B_{2})$ and replace it with an element of $B_{2}$, there by decreasing
the cardinality of this set by one. Eventually, we will end up with the case
that $B_{1} \subseteq I \cup B_{2}$, so $B_{1} \setminus I \subseteq B_{2}$,
and we have already dealt with this case.

### An $\mathrm{I}$-matroid gives us a $\mathrm{B}$-matroid

Given an $\mathrm{I}$-matroid, $(E, \mc{I})$, let $\mc{B} = \max(\mc{I})$ be
the set of maximal elements in $\mc{I}$ with respect to inclusion. We claim
that $(E, \mc{B})$ is in fact a $\mathrm{B}$-matroid.

Indeed, property B.1 is clear, since $\mc{I} \neq \varnothing$. For the
exchange property, take any distinct bases $B_{1}, B_{2} \in \mc{B}$ and $x \in
B_{1}\setminus B_{2}$. Then, $B_{1}\setminus\{x\} \in \mc{I}$ by the hereditary
property. Thus, by the augmentation property we can augment $B_{1} \setminus
\{x\}$ with some $y \in B_{2}\setminus (B_{1}\setminus \{x\})$ to get a new
independent set $B_{3}$. It then suffices to show that $B_{3}$ is maximal. This follows by the following lemma.

\lemma{equicardinality of maximal independent subsets}{Let $B_{1}, B_{2}$
be two maximal independent subsets of an $\mathrm{I}$-matroid $(E, \mc{I})$.
Then, $|B_{1}| = |B_{2}|$.}

*Proof:* Suppose the contrary that $|B_{1}| < |B_{2}|$. By the augmentation
property, we can find some $y \in B_{2} \setminus B_{1}$ such that $B_{1} \cup
\{y\}$ remains independent. This contradicts the maximality of $B_{1}$.
$\square$

Maximality of $B_{3}$ then follows since any independent subset in $\mc{I}$
strictly containing $B_{3}$ must have cardinality strictly greater than
$|B_{3}| = |B_{2}|$, which contradicts the equicardinality of maximal
independent subsets.

## Definition in terms of circuits

Dual to the notion of a maximal independent subset is that of a minimal
dependent subset. That is, a subset whose proper subsets are all independent.

~~~
<span style="color:magenta;"> I will do this section later, since circuits is less relevant. </span>
~~~

## Definition in terms of flats*

Arguably, this definition would be the most useful for my purpose. The notion
of a flat generalises strata in a hyperplane arrangement. Let us first see how
this can be extracted from an $\mathrm{I}$-matroid.

\defn{rank}{Given an $\mathrm{I}$-matroid $(E, \mc{I})$ ans a subset $S
\subseteq E$, the *rank* of $S$, denoted by $r(S)$, is the cardinality of the
largest independent set contained in $S$.}

\example{If $(E, \mc{I})$ is a linear matroid, then $r(S)$ is the dimension of
the span of $S$.}

\defn{closure}{Keeping the above notations, the closure of $S$, denoted by
$\cl(S)$, is defined as the union $$ \cl(S) = \bigcup_{\substack{S \subseteq
F \subseteq E \\ r(S) = r(F)}} F$$ of all sets $F$ containing $S$ with the same
rank.}

\example{In the case of a linear matroid, $\cl(S)$ consists of all elements of
$E$ lying in the span of $S$.}

A nice way of looking at flats is via hyperplane arrangements. A hyperplane
arrangement $M$ in a vector space $V$ is a linear matroid over $V^{\ast}$ such
that the underlying set $E$ consists of no zeroes. A *strata* of the
arrangement is the vanishing locus of some members of $E$. Under this language,
a flat of $M$ is precisely a strata of the arrangement.

Of course,
