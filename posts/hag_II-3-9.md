+++
title = "Hartshorne Exercise II.3.9"
date = Date(2021, 4, 4)
+++
@def tags = ["hartshorne", "easy"]
@def desc = "Ex II.3.9: Some fibred products"

# Hartshorne Exercise II.3.9

**Ex II.3.9 a)** Let $k$ be a field, and $\A_{k}^{1} = \Spec k[x]$ be the
affine line over $k$. Show that $\A_{k}^{1} \times_{k} \A_{k}^{1} \cong
\A_{k}^{2}$, and show that the point set of the product is not the product of
the underlying point sets of the factors.

### Solution

We have
\begin{align}
\A_{k}^{1} \times_{k} \A_{k}^{1} &= \Spec k[x] \times_{k} \Spec k[y] \\
&\cong \Spec(k[x]\otimes_{k}k[y]) \\
&\cong \Spec k[x, y] \\
&= \A_{k}^{2}.
\end{align}

As a topological space, $\A_{k}^{1}$ contains a generic point, with the
remaining points being closed points corresponding to the Galois orbits in its
algebraic closure.

To show that the point set of $\A_{k}^{2}$ is not the set theoretic product, it
suffices to show that the induced function of sets $\A_{k}^{2} \to \A_{k}^{1}
\times \A_{k}^{1}$ is not an injection. Here, the target represents the product
in $\mathbf{Sets}$.

This is clear. For instance, both $\left\langle 0\right\rangle$ and
$\left\langle x + y\right\rangle$ from the source are mapped to $(\left\langle
0\right\rangle , \left\langle 0\right\rangle )$.

**Exercise II.3.9 b)** Let $k$ be a field, let $s$ and $t$ be indeterminates
over $k$. Then, $\Spec k(s)$, $\Spec k(t)$ and $\Spec k$ are all one-point
spaces. Describe the product scheme $\Spec k(s) \times_{\Spec k} \Spec k(t)$.

### Solution

This boils down to the computation of $R = k(s) \otimes_{k} k(t)$. We claim
that $R$ is the subring $$S = \left\{\frac{f(s, t)}{g(s)h(t)} \in k(s, t) \mid
f \in k[s, t], g \in k[s], h \in k[t]\right\} \subseteq k(s, t).$$ To show
this, note that the natural map $\varphi: R \to S$ takes
$(f_{1}(s)/g_{1}(s))\otimes_{k}(f_{2}(t)/g_{2}(t))$ to
$\frac{f_{1}(s)f_{2}(t)}{g_{1}(s)g_{2}(t)}$. We demonstrate a two sided inverse
$\psi: S \to R$ to $\varphi$. Given any $r = f(s, t)/(g(s)h(t)) \in S$, where
$f(s,
t) = \sum_{(i, j)}^{} a_{i, j}s^{i}t^{j}$, put $\psi(r) = \sum_{(i, j)}^{}
   (a_{i, j}s^{i}/g(s))\otimes_{k}(t^{j}/h(t))$. That $\psi$ is a morphism of
   rings boils down to a tedious but doable calculation. A similar calculation
   shows that $\psi$ is a two-sided inverse of $\varphi$, as desired.

Now, note that $S = k[s, t][U^{-1}]$, where $U$ is the multiplicative system in
$k[s, t]$ consisting of all nonzero polynomials expressible as the product
$f(s)g(t)$. Thus, $\Spec S$ consists of all prime ideals in $k[s, t]$ not
meeting $U$. We know that $k[s, t]$ has the following three types of prime
ideals:

1. the zero ideal,
2. an ideal of the form $\left\langle p(s, t)\right\rangle$ for some irreducible $t \in k[s, t]$,
3. an ideal of the form $\left\langle f(s), g(s, t)\right\rangle$ where $f$ is irreducible in $k[s]$ and $g$ is irreducible in $(k[s]/\left\langle f\right\rangle )[t]$.

The last class of ideals all has nonempty intersection with $U$. For the second
class, if $\left\langle p\right\rangle$ does not meet $U$ if and only if $p$ is
a irreducible polynomial of two variables. The zero ideal clearly does not meet
$U$. This gives $$ \Spec S = \left\{\left\langle p(s, t) \mid p = 0 \text{ or
} p \text{ contains both terms in $s$ and in $t$ }\right\rangle \right\},$$
which has infinitely many points.
