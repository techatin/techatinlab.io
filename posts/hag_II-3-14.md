+++
title = "Hartshorne Exercise II.3.14"
date = Date(2021, 4, 4)
+++
@def tags = ["hartshorne", "medium"]
@def desc = "Ex II.3.14: closed points are dense in schemes of finite type over a field"

# Hartshorne Exercise II.3.14

**Ex II.3.14** If $X$ is a scheme of finite type over a field $k$, show that
the closed points of $X$ are dense. Give an example to show that this is not
true for arbitrary schemes.

### Proof

Cover $X$ by finitely many affine subschemes of the form $\Spec A_{i}$, where
each $A_{i}$ is a finitely generated algebra over $k$. As finite union of
closure is finite closure of union, it suffices to show that the set of closed
points in $\Spec A_{i}$ is dense for each $i$.

The closed points in $\Spec A_{i}$ corresponds to the maximal ideals of
$A_{i}$. As $A_{i}$ is finitely generated over $k$, we know that it is
Jacobson, so every prime ideal of $A_{i}$ contains the Jacobson radical
$\mathfrak{R}(A_{i})$. Now, the closure of the closed points in $\Spec A_{i}$
is precisely $$ V(\bigcap_{\mfm \in \operatorname{mSpec}(A_{i})} \mfm)
= V(\mathfrak{R}(A_{i})) = \Spec A_{i}.$$ This shows that the set of closed
points is dense in $X$, as desired.

This approach allows us to only require $X$ be a scheme over a quasicompact
locally Jacobson scheme (for instance, $\Spec \Z$).

For the counterexample, consider the scheme $\Spec k[x]_{\left\langle
x\right\rangle }$. It has two points -- one closed and one generic -- and the
closure of the only closed point is not the whole space. In general, one can
consider the spectrum of any DVR, such as $\Spec \Z_{\left\langle
2\right\rangle }$.
