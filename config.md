<!--
Add here global page variables to use throughout your
website.
The website_* must be defined for the RSS to work
-->
@def website_title = "The Confused (aspiring) Algebraic Geometer"
@def website_descr = "Bunch of stuffs about algebraic geometry"
@def website_url   = "https://techatin.gitlab.io/"

@def author = "XC"

@def mintoclevel = 2

<!--
Add here files or directories that should be ignored by Franklin, otherwise
these files might be copied and, if markdown, processed by Franklin which
you might not want. Indicate directories by ending the name with a `/`.
-->
@def ignore = ["node_modules/", "franklin", "franklin.pub"]

<!--
Add here global latex commands to use throughout your
pages. It can be math commands but does not need to be.
For instance:
* \newcommand{\phrase}{This is a long phrase to copy.}
-->
\newcommand{\A}{\mathbb{A}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\div}{\operatorname{div}}
\newcommand{\R}{\mathbb R}
\newcommand{\scal}[1]{\langle #1 \rangle}
\newcommand{\N}{\mathbb N}
\newcommand{\Os}{\mathcal O}
\newcommand{\Pb}{\mathbb P}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Spec}{\operatorname{Spec}}
\newcommand{\Frac}{\operatorname{Frac}}
\newcommand{\Pic}{\operatorname{Pic}}
\newcommand{\im}{\operatorname{im}}

\newcommand{\capprod}{\scriptsize\cap\normalsize}
\newcommand{\cupprod}{\scriptsize\cup\normalsize}

\newcommand{\id}{\trianglelefteq}

\newcommand{\cl}{\operatorname{cl}}

\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mcL}{\mathcal{L}}

\newcommand{\mfa}{\mathfrak{a}}
\newcommand{\mfb}{\mathfrak{b}}
\newcommand{\mfc}{\mathfrak{c}}
\newcommand{\mfm}{\mathfrak{m}}
\newcommand{\mfn}{\mathfrak{n}}
\newcommand{\mfN}{\mathfrak{N}}
\newcommand{\mfp}{\mathfrak{p}}
\newcommand{\mfq}{\mathfrak{q}}
\newcommand{\mfr}{\mathfrak{r}}
\newcommand{\mfR}{\mathfrak{R}}
\newcommand{\mfU}{\mathfrak{U}}

\newcommand{\G}{\operatorname{G}}
\newcommand{\Sym}{\operatorname{Sym}}
\newcommand{\rank}{\operatorname{rank}}
\newcommand{\Bl}{\operatorname{Bl}}
\newcommand{\det}{\operatorname{det}}

\newcommand{\prop}[2]{
  @@proposition
  **Proposition**: (_!#1_)
  #2
  @@
}

\newcommand{\defn}[2]{
  @@definition
  **Definition**: (_!#1_)
  #2
  @@
}

\newcommand{\lemma}[2]{
  @@lemma
  **Lemma**: (_!#1_)
  #2
  @@
}

\newcommand{\pmat}[1]{\begin{pmatrix} #1 \end{pmatrix}}

\newcommand{\theorem}[2]{
  @@theorem
  **Theorem**: (_!#1_)
  #2
  @@
}

\newcommand{\example}[1]{
  @@example
  **Example**:
  #1
  @@
}

\newcommand{\figenv}[3]{
~~~
<figure style="text-align:center;">
<img src="!#2" style="padding:0;margin:auto;#3" alt="#1"/>
<figcaption>#1</figcaption>
</figure>
~~~
}
