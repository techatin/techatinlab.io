\documentclass[dvipsnames]{article}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{accents}
\usepackage[top=0.5em, bottom=0.5em, left=1.6in, right=1.6in]{geometry}
\usepackage{graphicx}
\usepackage{hyperref, cleveref}
\usepackage{ifthen}
\usepackage{mathrsfs}
\usepackage{microtype}
\usepackage[parfill]{parskip}
\usepackage{quiver}
\usepackage{tcolorbox}
\usepackage{pgfplots}
\usepackage{subfiles}
\usepackage{thmtools}
\usepackage{tikz}
\usepackage{xcolor}

\hypersetup{
    colorlinks=true,
    linkcolor=WildStrawberry
}

\pgfplotsset{compat=newest}
\pgfplotsset{yticklabel style = {font=\scriptsize, yshift=0.15cm}}
\pgfplotsset{xticklabel style = {font=\scriptsize, xshift=0.15cm}}
\pgfplotsset{tick style = {thick, black}, tickwidth=0.25cm}

% \setlength{\parindent}{0pt}
\newcommand{\question}[1]{\vspace{5mm}\begin{tcolorbox}[colback=white, colframe=black, boxrule=0.2mm, arc=0mm, hbox]\large{\textbf{#1}}\normalsize\end{tcolorbox}}
\newcommand{\subquestion}[1]{\large{\textbf{#1)}}\normalsize}
\newcommand{\deriv}[3]{\ifthenelse{\equal{#3}{1}}{\frac{d #1}{d #2}}{\frac{d^{#3}#1}{d #2^{#3}}}}
\newcommand{\As}{\mathbb{A}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\Cs}[1]{\mathbb{C}^{#1}}
\newcommand{\Lrr}{\mathcal{L}}
\newcommand{\Pb}{\mathbb{P}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\Rs}[1]{\mathbb{R}^{#1}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Np}{\mathbb{N}^+}
\newcommand{\pmat}[1]{\begin{pmatrix}#1\end{pmatrix}}

\newcommand{\capprod}{\,{\scriptstyle\cap}\,}
\newcommand{\cupprod}{\,{\scriptstyle\cup}\,}

% Ideals
\newcommand{\mf}[1]{\mathfrak{#1}}
\newcommand{\mfa}{\mf{a}}
\newcommand{\mfb}{\mf{b}}
\newcommand{\mfc}{\mf{c}}
\newcommand{\mfm}{\mf{m}}
\newcommand{\mfn}{\mf{n}}
\newcommand{\mfp}{\mf{p}}
\newcommand{\mfq}{\mf{q}}
\newcommand{\mfN}{\mf{N}}
\newcommand{\mfR}{\mf{R}}
\newcommand{\id}{\trianglelefteq}

\newcommand{\Os}{\mathcal{O}}

% operators
\DeclareMathOperator{\Aut}{Aut}
\DeclareMathOperator{\Bl}{Bl}
\DeclareMathOperator{\di}{div}
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\Spec}{Spec}
\DeclareMathOperator{\tr}{tr}

\newenvironment{amatrix}[1]{%
  \left(\begin{array}{@{}*{#1}{c}|c@{}}
}{%
  \end{array}\right)
}

\declaretheorem[name=Theorem, numberwithin=section]{thm}
\declaretheorem[name=Lemma, sibling=thm]{lem}
\declaretheorem[name=Proposition, sibling=thm]{prop}
\declaretheorem[name=Corollary, sibling=thm]{cor}
\declaretheorem[name=Example, sibling=thm]{eg}
\declaretheorem[name=Remark, sibling=thm, style=remark]{rmk}
\declaretheorem[name=Definition, sibling=thm, style=definition, qed=$\diamond$]{defn}
\declaretheorem[name=Blackbox, sibling=thm, shaded={bgcolor=Melon, margin=0.05\textwidth, textwidth=0.9\textwidth}]{blackbox}

\newcommand{\fref}[1]{[\autoref{#1}, \texttt{\nameref*{#1}}]}

\let\oldvec\vec
\renewcommand{\vec}[1]{\mathbf{#1}}

\let\oldphi\phi
\renewcommand{\phi}{\varphi}

\let\oldwidetilde\widetilde
\renewcommand{\widetilde}[1]{\ensuremath\accentset{\sim}{#1}}

\title{Chow Ring of a Blowup}
\author{}
\date{}

\begin{document}

\maketitle

Let $X$ be a nonsingular variety over an algebraically closed field $k$, and
$i: Y \to X$ a nonsingular subvariety of codimension $d$. We wish to describe the Chow ring of $\Bl_{Y}X$. The end goal, of course, is to have some grasp of what happens when we blow up successively the strata of a hyperplane arrangement on $\Pb^{n}$.

\tableofcontents

\section{The set-up}

For ease of notation, write $\widetilde{Y} = \Bl_{X}Y$ and $\widetilde{X}
= \Pb(\mathscr{N}_{Y})$ the exceptional divisor. Thus, we have the fibre
diagram as shown below. We also have open embeddings $X \xleftarrow{\psi}
X\setminus Y = \widetilde{X}\setminus\widetilde{Y}
\xrightarrow{\widetilde{\psi}} \widetilde{X}$ compatible with the relevant
blowup map. \[\begin{tikzcd}
	{\widetilde{Y}} & {\widetilde{X}} \\
	Y & X
	\arrow["g"', from=1-1, to=2-1]
	\arrow["i"', from=2-1, to=2-2]
	\arrow["f", from=1-2, to=2-2]
	\arrow["j", from=1-1, to=1-2]
	\arrow["\lrcorner"{anchor=center, pos=0.125}, draw=none, from=1-1, to=2-2]
\end{tikzcd}\]


\section{The Chow group}

The first step is to describe the group-theoretic structure of
$A^{\ast}(\widetilde{X})$. We can do this using the following commutative
diagram, whose rows are the excision short exact sequences. \[\begin{tikzcd}
	{A^{\ast-d}(Y)} & {A^{\ast}(X)} & {A^{\ast}(X\setminus Y)} & 0 \\
	{A^{\ast-d}(\widetilde{Y})} & {A^{\ast}(\widetilde{X})} & {A^{\ast}(\widetilde{X}\setminus \widetilde{Y})} & 0
	\arrow[from=1-3, to=1-4]
	\arrow[from=2-3, to=2-4]
	\arrow["{\psi^\ast}", from=1-2, to=1-3]
	\arrow["{\widetilde{\psi}^\ast}"', from=2-2, to=2-3]
	\arrow["{i_\ast}", from=1-1, to=1-2]
	\arrow["{j_\ast}"', from=2-1, to=2-2]
	\arrow["{g^\ast}"', hook, from=1-1, to=2-1]
	\arrow["{f^\ast}"', from=1-2, to=2-2]
	\arrow[shift right=1, no head, from=1-3, to=2-3]
	\arrow[no head, from=1-3, to=2-3]
\end{tikzcd}\]

We thus claim the following.

\begin{prop}[Chow group of $\tilde{X}$]

We have $$ A^{\ast}(\widetilde{X}) = j_{\ast}A^{\ast-d}(\widetilde{Y}) +
f^{\ast}A^{\ast}(X).$$

\end{prop}

\begin{proof}
Take any element $x \in A^{\ast}(\widetilde{X})$. We can find some
$\beta \in A^{\ast}(X)$ such that $\psi^{\ast}(\beta)
= \widetilde{\psi}^{\ast}(x)$, identifying $X\setminus Y$ with
$\widetilde{X}\setminus \widetilde{Y}$. Thus, $\widetilde{\psi}^{\ast}(x
- f^{\ast}(\beta)) = 0$, so $x - f^{\ast}(\beta) = j_{\ast}(\alpha)$ for some
$\alpha \in A^{\ast-d}(\widetilde{Y})$, as desired.
\end{proof}

\section{The ring structure}

Hence, to specify the ring structure on $A^{\ast}(\widetilde{X})$, it suffices
to find, for all $x, x^{\prime} \in A^{\ast}(X)$ and $y, y^{\prime} \in
A^{\ast-d}(\widetilde{Y})$, the expressions for
\begin{itemize}
    \item $f^{\ast}(x) \capprod f^{\ast}(x^{\prime})$,
    \item $j_{\ast}(y)\capprod j_{\ast}(y)$, and
    \item $f^{\ast}(x) \capprod j_{\ast}(y)$.
\end{itemize}

Since pullback is a ring homomorphism, we simply have $$ f^{\ast}(x\capprod
x^{\prime}) = f^{\ast}(x) \capprod f^{\ast}(x^{\prime}).$$ By the projection
formula, we also have $$ j_{\ast}(y) \capprod f^{\ast}(x) = j_{\ast}(y \capprod
j^{\ast}f^{\ast}(x)) = j_{\ast}(y \capprod g^{\ast}i^{\ast}x).$$ The remaining
case, however, is slightly more complicated and would require us to recall the
\emph{excess intersection formula}.

\begin{thm}[excess intersection formula]
    Consider the fibre diagram shown below. \[\begin{tikzcd}
	{X^{\prime\prime}} & {Y^{\prime\prime}} \\
	{X^\prime} & {Y^\prime} \\
	X & Y
	\arrow["i"', from=3-1, to=3-2]
	\arrow["{i^\prime}", from=2-1, to=2-2]
	\arrow[from=1-1, to=1-2]
	\arrow["q"', from=1-1, to=2-1]
	\arrow["g"', from=2-1, to=3-1]
	\arrow["p", from=1-2, to=2-2]
	\arrow["f", from=2-2, to=3-2]
\end{tikzcd}\]


There is a canonical embedding of $\mathscr{N}_{i^{\prime}}$ in
$g^{\ast}\mathscr{N}_{i}$, thus defining a quotient bundle $\mathscr{E}$ on
$X^{\prime}$, called the *excess normal bundle*. The refined Gysin
homomorphisms $i^{!}$ and $i^{\prime, !}$ are then related by the formula
$$i^{!}(\alpha) = c_{e}(q^{\ast}\mathscr{E})\capprod i^{\prime, !}(\alpha)$$
for all $\alpha \in A_{\ast}Y^{\prime\prime}$.

\end{thm}

The formula allows us to prove a slightly more general result about the
compatibility between the intersection product and pushforward along closed embeddings

\begin{prop}[push forward and intersection products]

    Consider the fibre square \[\begin{tikzcd} {X^\prime} & {Y^\prime} \\ X & Y
    \arrow["f"', from=2-1, to=2-2] \arrow["{f^\prime}", from=1-1, to=1-2]
    \arrow["h"', from=1-1, to=2-1] \arrow["g", from=1-2, to=2-2]
    \end{tikzcd},\] where $g$ is a closed embedding with codimension $d$ and
    normal bundle $\mathscr{N}$. Then, we have $$ h_{\ast}(x) \cdot_{f}
g_{\ast}(y) = h_{\ast}(c_{d}(f^{\ast}\mathscr{N})\capprod (x\cdot_{f^{\prime}}
y)).$$ 
\end{prop}

Before the proof, we briefly recall the construction of a refined intersection
product. Given morphisms $f\colon X \to Y$, $p_{X}\colon X^{\prime} \to X$ and
$p_{Y}\colon Y^{\prime} \to Y$, we can form the refined intersection product
$\cdot_{f}\colon A_{\ast}X^{\prime} \otimes A_{\ast}Y^{\prime} \to A_{\ast -
n}(X^{\prime}\times_{Y}Y^{\prime})$ by setting $$ x\cdot_{f} y =
\gamma_{f}^{!}(x\times y),$$ where $x \in A_{\ast}X^{\prime}$, $y \in
A_{\ast}Y^{\prime}$, and $\gamma_{f}^{!}$ is the refined Gysin homomorphism
associated to the fibre diagram \[\begin{tikzcd} {X^\prime \times_Y Y^\prime} &
        {X^\prime \times Y^\prime} \\ X & {X\times Y} \arrow["{\gamma_f}"',
        from=2-1, to=2-2] \arrow[from=1-1, to=2-1] \arrow[from=1-1, to=1-2]
\arrow["{p_X\times p_Y}", from=1-2, to=2-2] \arrow["\lrcorner"{anchor=center,
pos=0.125}, draw=none, from=1-1, to=2-2] \end{tikzcd}.\]

\end{document}
