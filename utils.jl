function hfun_bar(vname)
    val = Meta.parse(vname[1])
    return round(sqrt(val), digits=2)
end

function hfun_m1fill(vname)
    var = vname[1]
    return pagevar("index", var)
end

function lx_baz(com, _)
    # keep this first line
    brace_content = Franklin.content(com.braces[1]) # input string
    # do whatever you want here
    return uppercase(brace_content)
end

function hfun_display_tag_header()
    tags = locvar("tags")
    if length(tags) == 0
        return ""
    else
        return "<h2>Tags</h2>"
    end
end

function hfun_list_tags()
    tags = locvar("tags")
    html_tags = map(x -> "<li><a href=\"../../tag/$x\", rel=\"nofollow noopener noreferrer\"><i class=\"fas fa-fw fa-tag\" aria-hidden=\"true\"></i> $x </a></li>", tags)
    return join(html_tags, "\n")
end

function hfun_recent_posts(m::Vector{String})
  @assert length(m) == 1 "only one argument allowed for recent posts (the number of recent posts to pull)"
  n = parse(Int64, m[1])
  list = readdir("posts")
  filter!(f -> endswith(f, ".md") && f != "index.md" , list)
  markdown = ""
  posts = []
  df = DateFormat("mm/dd/yyyy")
  for (k, post) in enumerate(list)
      fi = "posts/" * splitext(post)[1]
      title = pagevar(fi, :title)
      date = pagevar(fi, :date)
      desc = pagevar(fi, :desc)
#       date = Date(pagevar(fi, :date), df)
      push!(posts, (title=title, link=fi, date=date, desc=desc))
  end

  # pull all posts if n <= 0
  n = n >= 0 ? n : length(posts)+1

  for ele in sort(posts, by=x->x.date, rev=true)[1:min(length(posts), n)]
      markdown *= "* [($(ele.date)) $(ele.title)](../$(ele.link))\n > $(ele.desc)\n"
  end

  return fd2html(markdown, internal=true)
end

function hfun_list_all_tags()
    list = readdir("posts")
    filter!(f -> endswith(f, ".md") && f != "index.md" , list)
    all_tags = []

    for post in list
        fi = "posts/" * splitext(post)[1]

        try
            tags = pagevar(fi, "tags")
            for tag in tags
                if !(tag in all_tags)
                    push!(all_tags, tag)
                end
            end
        catch e
            continue
        end
    end
    html_tags = map(x -> "<li><a href=\"../../tag/$x\", rel=\"nofollow noopener noreferrer\"><i class=\"fas fa-fw fa-tag\" aria-hidden=\"true\"></i> $x </a></li>", all_tags)
    return join(html_tags, "\n")
end
  
